# ECS1 2020-2021

## COURS

0. [Bases](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyBases20.pdf)
1. [Ensembles et applications](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolySets20.pdf)
1. [Nombres complexes](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyComplexes20.pdf)
1. [Suites - Partie 1](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyReels20.pdf)
1. [Calcul matriciel](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyMatrix.pdf)
1. [Résolution de systèmes](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolySystemes.pdf)
1. [Introduction aux espaces vectoriels](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyEV.pdf)
1. [Dénombrements](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyDenomb.pdf)
1. [Probabilités sur un espace fini](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyProbasIntro.pdf)
1. [Limites et continuité](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyLimCont20.pdf)
1. [Étude globale des fonctions](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyGlobale20.pdf)
1. [Dérivation](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyDeriv20.pdf)
1. [Intégration](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyInteg20.pdf)
1. [Polynômes](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyPoly20.pdf)
1. [VAR finies](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyVAR.pdf)
1. [Dérivées successives](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyTaylor.pdf)
1. [Étude asymptotique des suites](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyComparaisonSuites20.pdf)
1. [Séries](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolySeries20.pdf)
1. [Espaces vectoriels de dimension finie](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyEVfinis20.pdf)
1. [Homomorphismes d'EV](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/Poly_AppLin20.pdf)
1. [Espaces probabilisés discrets](COURS/20_probas_dis_cours.md)


1. [Poly -> chapitre 19](https://gitlab.com/GiYoM/ecs1/-/blob/master/2020_21_ECS1/Poly/PolyECS2020_21.pdf)


## EXERCICES

20.  [Espaces probabilisés discrets](/EXERCICES/20_probas_dis_exos.md)


## COLLES

1. [Colle 21](/COLLES/Colle21_20.md)

## DS

