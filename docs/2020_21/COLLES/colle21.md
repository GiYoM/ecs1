---
hide:
  #- navigation # Hide navigation
  - toc # Hide table of contents
---


# Colle 21

**Semaine distanciée du 26 au 30 avril 2021**



## Cours

### Applications linéaires

Homomorphisme d'EV - Image d'une CL - Caractérisation des applications linéaires
- $\mathscr L(\mathbf{V},\mathbf{W})$ - Noyau, lien avec l'injectivité - Image, lien
avec  la surjectivité  -  Endo/iso/auto-morphismes  - Projections/Projecteurs  -
**Dimension finie** Matrice d'une AL - Image d'u - Détermination d'un homomorphisme à l'aide d'une base
- Espaces isomorphes - Rang d'une  AL - Injectivité et surjectivité en dimension
finie - Hyperplan et noyau d'une forme linéaire.


### Matrices et applications linéaires

Matrice d'une  famille - Image d'un  vecteur et produit matriciel  - Application
canoniquement  associée à  une  matrice  - Correspondance  entre  points de  vue
vectoriel  et matriciel  sur  les  homomorphismes -  Rang  d'une  matrice -  Cas
particulier des endomorphismes - Polynômes d'endomorphismes - 

## Questions de cours 

- Soit $f$  l'application linéaire définie de $ℝ^2$ dans  $ℝ^2$ par: $ \forall
  (x,y) \in ℝ² $,  $ f((x,y)) = ( 4x -  6y , 2x - 3y ) $.  Démontrer que $f$ est
  une projection en précisant ses caractéristiques.
- S'il existe une base de $\mathbf{V}$ dont l'image par $\ell$ est une base de
  $\mathbf{W}$ alors $\ell$ est un isomorphisme.
- Théorème du rang.
- Caractérisation de l'injectivité et de la surjectivité en
  dimension finie.
- Soit $φ∈\mathscr{L}(ℝ^3)$ l'endomorphisme canoniquement associé à la matrice $M=
    \begin{bmatrix}
      0&1&1\\
      1&0&-1\\
      -1&1&2
    \end{bmatrix}$.

    Démontrer  que $φ$  est un  projecteur. Déterminez  ${\rm Ker}(φ)$  et ${\rm
    Ker}(φ-{\rm Id}_{ℝ^3})$.
