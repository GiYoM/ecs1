
<!--
### Essai ABR


{{ bst([2, 9, 3, 7, 4, 5, 1, 8])}}
-->


-  Vous  pouvez  taper votre  code  en  utilisant  la  touche ++tab++  pour  les
  indentations. La fenêtre s'agrandit au fur et à mesure de la frappe.
- Il  y a un  `#!python print` par  défaut de la  dernière commande donc  ne pas
  utiliser `#!python print` dans votre code.
- le bouton `Nettoyer` efface la fenêtre des résultats.
- le  bouton `Afficher/Masquer` permet de  basculer entre l'affichage et  le non
  affichage des commandes dans la fenêtre des résultats.


{{ pyodide()}}





- Le javascript dans `/javascript/pyodid.js`:

```javascript

/////////////////////////////////////////////////////
//
//        P Y O D I D E
//
//
////////////////////////////////////////////////////////

//////   POUR AVOIR DES TEXTAREAS QUI S'ÉTENDENT AVEC LE TEXTE

// Targets all textareas with class "txta"
let textareas = document.querySelectorAll('.txta'),
    hiddenDiv = document.createElement('div'),
    content = null;

// Adds a class to all textareas
for (let j of textareas) {
  j.classList.add('txtstuff');
}

// Build the hidden div's attributes

// The line below is needed if you move the style lines to CSS
 hiddenDiv.classList.add('hiddendiv');

// Add the "txta" styles, which are common to both textarea and hiddendiv
// If you want, you can remove those from CSS and add them via JS
hiddenDiv.classList.add('txta');

// Loop through all the textareas and add the event listener
for(let i of textareas) {
  (function(i) {
    // Note: Use 'keyup' instead of 'input'
    // if you want older IE support
    i.addEventListener('input', function() {
      
      // Append hiddendiv to parent of textarea, so the size is correct
      i.parentNode.appendChild(hiddenDiv);
      
      // Remove this if you want the user to be able to resize it in modern browsers
      i.style.resize = 'none';
      
      // This removes scrollbars
      i.style.overflow = 'hidden';

      // Every input/change, grab the content
      content = i.value;

      // Add the same content to the hidden div
      
      // This is for old IE
      content = content.replace(/\n/g, '<br>');
      
      // The <br ..> part is for old IE
      // This also fixes the jumpy way the textarea grows if line-height isn't included
      hiddenDiv.innerHTML = content + '<br style="line-height: 3px;">';

      // Briefly make the hidden div block but invisible
      // This is in order to read the height
      hiddenDiv.style.visibility = 'hidden';
      hiddenDiv.style.display = 'block';
      i.style.height = hiddenDiv.offsetHeight + 'px';

      // Make the hidden div display:none again
      hiddenDiv.style.visibility = 'visible';
      hiddenDiv.style.display = 'none';
    });
  })(i);
}


////////  PYODIDE

const output = document.getElementById("output");
const code = document.getElementById("code");
let cpt = 0;

function addToOutput(s) {
   cpt += 1;
   // output.value += 'In  ['+ cpt+ ']: ' + code.value + '\n';
   output.value += 'Out [' + cpt+ ']: ' + s + '\n';
}

function clearOutput(s) {
   // output.value += 'In  ['+ cpt+ ']: ' + code.value + '\n';
   output.value = '';
}

function afficheCommande(s) {
      cpt += 1;
    output.value += 'In  ['+ cpt+ ']: ' + code.value + '\n';
    output.value += 'Out [' + cpt+ ']: ' + s + '\n';
}

var clicked = false;
function clique(event){
    clicked = !clicked;
}

// pour avoir des tabulations dans le textarea du code
document.getElementById('code').addEventListener('keydown', function(e) {
  if (e.key == 'Tab') {
    e.preventDefault();
    var start = this.selectionStart;
    var end = this.selectionEnd;

    // set textarea value to: text before caret + tab + text after caret
    this.value = this.value.substring(0, start) +
      "\t" + this.value.substring(end);

    // put caret at right position again
    this.selectionStart =
      this.selectionEnd = start + 1;
  }
});

document.getElementById('affiche').addEventListener("click", clique);

output.value = 'Un instant Dave, je me prépare...\n';
 // init Pyodide
async function main(){
   await loadPyodide({ indexURL : 'https://cdn.jsdelivr.net/pyodide/v0.17.0/full/' });
   output.value += 'Je suis prêt Dave !\n';
 }
 let pyodideReadyPromise = main();

 async function evaluatePython() {
   await pyodideReadyPromise;
   try {
       let output = await pyodide.runPythonAsync(code.value);
       if (clicked) {
	   afficheCommande(output);
       } else {
	   addToOutput(output);
       }
   } catch(err) {
	 addToOutput(err);
   }
 }


///////////////////////////////////////////////////////
//
//         fin Pyodide
//
////////////////////////////////////////////////////////

```


- Pour avoir la `textarea` extensible (dans `/stylesheet/extra.css`):


```css

/* The .hiddendiv styles are added with JS.
   You can remove the JS lines and use CSS instead*/
.hiddendiv {
  display: none;
  white-space: pre-wrap;
  word-wrap: break-word;
}

/* the styles for 'txta' are applied to both the textarea and the hidden clone */
/* these must be the same for both */
.txta {
  width: 100%;
  max-width: 500px;
  min-height: 100px;
  /*font-family: Arial, sans-serif;
  font-size: 16px;*/
  overflow: hidden;
  line-height: 1.4;
}
```

- la macro dans `main.py`

```python
def pyodide0():
    s ="<div>Code:</div><textarea placeholder=\"Tapez votre code ici\" id='code' class=\"txta\" ></textarea><button onclick='evaluatePython()'>Exécuter</button><button onclick='clearOutput()'>Nettoyer</button> <button id='affiche'>Afficher/Masquer</button> <div>Résultat:</div><textarea id='output' class=\"txta common\" ></textarea>"
    return s
	
def define_env(env):
  @env.macro
  def pyodide():
      return pyodide0()
```




qui correspond à ce code `html`:

```html
<div>
Code:
</div>
<textarea placeholder="Tapez votre code ici" id='code' class="txta" ></textarea>

 <button onclick='evaluatePython()'  id='rouge'>Exécuter</button>
 <button id='rouge' onclick='clearOutput()'>Nettoyer</button> 
 <button id='affiche'>Afficher/Masquer</button> 
 
<div>
Résultat:
</div>
<textarea id='output' class="txta common" ></textarea>
```

et il ne reste qu'à taper `pyodide()` entre deux accolades pour avoir la console
dans son fichier `md`.
