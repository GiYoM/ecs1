%% Last modified: <EVfinis20.tex modifié par  Guillaume CONNAN le mardi 23 mars 2021 à 14h 13min 25s>

\chapterimage{./IMG/mib}
\chaptertext{Un  chapitre  aride  rempli  de définitions  et  de  résultats  qui
  serviront   de   point   de   départ   des   prochains   chapitres   d'algèbre
  linéaire.  On y trouvera beaucoup de démonstrations aussi, très éloignées des
  mathématiques du  lycée, qui demanderont  un certain effort  intellectuel pour
  être appréhendées.  Il s'agira en fait du premier vrai test de ce début de second semestre:
  êtes-vous rentré dans votre nouvelle peau de préparationnaire? Are you ready?}



\chapter{Espaces vectoriels de dimension finie}

\minitoc

En  l'absence d'autre  précision, on  considèrera  dans la  suite que  E est  un
$\bbk$-espace vectoriel.

\section{Bases d'un espace vectoriel de dimension finie}


\subsection{Définition}

Tout d'abord, définissons la notion d'espace vectoriel de dimension finie:

\begin{definition}[Espace vectoriel de dimension finie]
  On dit  qu'un $\bbk$-espace vectoriel  est de dimension finie  lorsqu'il admet
  une famille génératrice finie.
\end{definition}

Vous aurez remarqué qu'on définit les espaces vectoriels de dimension finie sans
avoir défini ce qu'était la dimension d'un espace vectoriel...patience!



\subsection{Théorème de la base incomplète}


Avant d'aborder  ce théorème  fondamental que les  ECS ont  l'honneur d'étudier,
occupons-nous d'un petit lemme qui nous permettra de nous remettre dans le bain.

\begin{theoreme}\label{lemme::fl}
  Soit $(e_i)_{i∈I}$  une famille  \textbf{libre} de  E et $v$  un élément  de E
  telle que la famille $(e_i)_{i∈I}∪\{v\}$ soit \textbf{liée}.

  Alors  $v$  est   une  \textbf{combinaison  linéaire}  des  vecteurs  de
  $(e_i)_{i∈I}$ et ce de manière unique.
\end{theoreme}

Comme  c'est un  échauffement,  il serait  de  bon ton  de  savoir démontrer  ce
théorème en partant d'une combinaison linéaire nulle bien choisie:
$\ds\sum_{i∈I}λ_i\cdot e_i+λ\cdot v=0_E$.


Et  maintenant, voici  notre  vedette  dont je  vous  propose une  démonstration
minimaliste donc empreinte d'une certaine beauté...

\begin{theoreme}[Théorème de la base incomplète]\label{thm::base_inc}
  Soit $E$  un espace  vectoriel non réduit  à $\bigl\{0_E\bigr\}$  de dimension
  finie. Il existe  donc une famille $\GR=(g_i)_{i∈I}$ génératrice finie  de E telle
  que $\#I>0$.

  Toute sous-famille $\FR=(g_i)_{i∈L}$ \textbf{libre} de $\GR$ peut être complétée en
  une  famille  $\BR=(g_i)_{i∈M}$  de  $\GR$  qui  soit  une  \textbf{base}  de  E,  avec
  $L\subseteq M\subseteq I$.

  $$\underbrace{\underbrace{\underbrace{g_1,g_2,...,g_\ell}_{\FR \text{ libre }},g_{\ell
        + 1},...,g_m}_{\BR \text{ base }},g_{m+1},...,g_i}_{\GR \text{ génératrice}}$$
\end{theoreme}


\bclampe Introduire l'ensemble $\bbf=\bigl\{\# M\ \mid\ L\subseteq M\subseteq I
\land (g_i)_{i∈M} \text{ est libre }\bigr\}$ puis étudier son maximum.

\smallskip

$$\underbrace{\underbrace{\boxed{\textcolor{gray}{\overbrace{g_1,g_2,\dots,g_{\ell}}^{\text{Famille
            libre de départ}}},\dots,g_{p}}}_{\BR
    \text{ famille libre
      maximale}},g_{p+1},\dots,\boxed{g_k},\dots,g_i}_{\text{ toute surfamille }\BR∪\{g_k\}\text{ est liée}}$$


Cet ensemble $\bbf$ contient $\# L=\ell$ donc est non vide.

Il est majoré par $\# I=i$.

L'ensemble de nombres entiers naturels $\bbf$ étant non vide et majoré, il admet
un plus grand élément (un maximum) que nous noterons $p$.

$$\bbf=\llbracket \ell,p \rrbracket $$


Cela signifie qu'il existe un ensemble $M_p$ tel que  $ L\subseteq M_p\subseteq I$
et $\BR=(g_i)_{i∈M_p}$ est libre et $M_p$ est l'ensemble de cardinal maximum vérifiant
cette propriété.


Donc si  on considère  un entier  $k$ qui  est dans  $I\setminus M_p$,  alors la
famille $(g_i)_{i∈M_p∪\{k\}}$ est liée alors  que la famille $(g_i)_{i∈M_p}$ est
libre.

D'après       le      théorème       \vref{lemme::fl},      on       a      donc
$g_k∈\Vect\left(\BR\right)$ pour tout $g_k$ appartenant à $\GR\setminus \BR$.

Ainsi $\Vect(\GR\setminus \BR) \subseteq \Vect(\BR)$ or on a bien sûr $\Vect(\BR) \subseteq
\Vect(\BR)$ donc $\Vect(\GR) \subseteq \Vect(\BR)$.

Mais $\BR\subseteq \GR$ donc $\Vect(\BR) \subseteq \Vect(\GR)$.

Finalement on obtient que $\Vect(\BR) = \Vect(\GR \ (= E))$: la famille $\BR$ est donc
génératrice. Or c'est une famille libre. C'est donc une base de E.


$\BR$ est une famille qui complète la famille libre $\FR$ afin de former une base.

\smallskip

Ouch, ça c'est de la démo.




\begin{theoreme}[Corrolaire]\label{thm::existence}
  Tout $\bbk$-espace vectoriel E \textbf{non nul} de \textbf{dimension finie}
  admet une base (de longueur) finie.
\end{theoreme}

\bclampe  E  étant  de  dimension  finie admet  une  famille  génératrice  finie
$(g_i)_{i∈I}$ qui constient au moins un élément $g_0$ non nul.
On peut donc appliquer le  théorème \vref{thm::base_inc} à la sous-famille libre
$(g_0)$. Réflechissez-y...





\subsection{Lemme de \sout{Steinitz} Grassmann}

Nous avons déjà parlé du pauvre  Grassmann, modeste professeur du secondaire qui
révolutionna les  mathématiques avec son  étude des espaces vectoriels  mais qui
fut totalement ignoré et méprisé. Le lemme suivant en est un exemple cuisant car
il porte le  nom de Steinitz qui  reprit les travaux de son  aîné et compatriote
Grassmann sans bien sûr jamais le citer.  

\smallskip

Encore une  grosse démonstration  à venir  et tout le  reste coulera  ensuite de
source.  Le théorème de la base incomplète nous dit que \textit{s'il existe} une
sous-famille libre $\FR$
d'une famille génératrice $GR$, \textit{alors} on  peut la compléter en une base
$\BR$ avec $\FR  \subseteq \BR \subseteq \GR$ avec $\BR$  la plus grande famille
libre possible.

Mais nous  voudrions un résultat plus  général, avec des familles  non forcément
reliées par inclusion.


\begin{theoreme}
  Soit A un sous-espace vectoriel non nul de E.

  Si A admet une famille génératrice de cardinal $n∈ℕ^*$, TOUTE famille de A de 
  cardinal $n+1$  est liée.
  
  
\end{theoreme}


\bclampe Il  s'agit de  démontrer ce  résultat par  récurrence sur  le cardinal
$n∈ℕ^*$ de la famille génératrice.




Je vous laisse traiter l'initialisation.

Soit  maintenant   Α  un   sev  non  nul   admettant  une   famille  génératrice
$(g_i)_{i∈\llbracket 1,n+1 \rrbracket }$.

Considérons   une  famille   \textit{quelconque}   de  $n+2$   éléments  de   A:
$(a_i)_{i∈\llbracket 1,n+2 \rrbracket }$. 

Alors pour chaque $k∈\llbracket 1,n+2 \rrbracket $ on peut écrire:

$$a_k=\underbrace{\sum_{i=1}^nλ_ig_i}_{b_k} + α_kg_{n+1}\quad (1)$$

\begin{itemize}
\item si tous les $α_k$ sont  nuls, alors les $n+2$ vecteurs $a_k$ appartiennent
  à    $\Vect(g_i,   i∈\llbracket    1,n   \rrbracket    )$.   Alors,    d'après
  \textit{l'hypothèse  de   récurrence},  on   peut  conclure  que   la  famille
  $(a_i)_{i∈\llbracket 1,n+2 \rrbracket }$ est liée.

\item  sinon, il  existe  au moins  un  coefficient non  nul,  disons $a_1$  par
  exemple. Alors $g_{n+1}=\frac{1}{α_1}(a_1-b_1)$.

  En substituant dans la relation (1) on obtient:

  $$(∀k∈\llbracket 2,n+2 \rrbracket )(a_k=b_k+\frac{α_k}{α_1}(a_1-b_1))$$

  On en déduit que

  $$a_k-\frac{α_k}{α_1}a_1=b_k-\frac{α_k}{α_1}b_1$$

  Les $n+1$  vecteurs $a_k-\frac{α_k}{α_1}a_1$ appartiennent donc  à $\Vect(g_i,
  i∈\llbracket 1,n \rrbracket )$ comme combinaison linéaire de $b_k$ et $b_1$.  Alors, d'après 
  \textit{l'hypothèse de  récurrence}, on peut  conclure que ces  $n+1$ vecteurs
  sont liés.

  Il existe donc des scalaires $μ_k$ \textit{non tous nuls} tels que $$\sum_{k=2}^{n+2}μ_k\left(a_k-\frac{α_k}{α_1}a_1\right)=0_E$$

  Alors

  $$-\left(\sum_{k=2}^{n+2}μ_k \frac{α_k}{α_1}\right)a_1+\sum_{k=2}^{n+2}μ_ka_k=0_E$$

  ce qui achève la preuve car on  a construit une combinaison linéaire nulle des
  $a_k$ dont les scalaires ne sont pas tous nuls.
  
  
\end{itemize}


Ce théorème nous permet d'énoncer un corrolaire immédiat qui sera extrêmement utile:

\begin{theoreme}[Corrolaire]

  Si E admet une famille génératrice de cardinal $n$, alors :
  \begin{itemize}
  \item toute famille d'au moins $n+1$ vecteurs est liée;
    \item toute famille libre est de cardinal au plus $n$.
  \end{itemize}
  
\end{theoreme}

\subsection{Dimension d'un espace vectoriel}


\begin{theoreme}[Théorème de la dimension]\label{thm::dim}
  Dans un $\bbk$-espace vectoriel E de  dimension finie, toutes les bases ont la
  même dimension.
  
\end{theoreme}

\bclampe  Considérez deux  bases quelconques:  on peut  en particulier  dire que
l'une est génératrice et l'autre libre et vice-versa et on utilise le corrolaire précédent.


\begin{definition}[Dimension d'un $\bbk$-EV de dimension finie]
  Si  E  est  un  $\bbk$-espace   vectoriel  de  dimension  finie,  le  cardinal
  d'\textit{une} base est appelé \textbf{dimension} de E. On le note $\dim(E)$.
\end{definition}

\begin{remarques}
  \begin{itemize}
  \item    L'existence   d'une    base   est    assurée   par    le   corrolaire
    \vref{thm::existence}
  \item Si $\dim(E)=n∈ℕ^*$  alors toute famille d'au moins $n+1$  vecteurs est liée,
    toute famille libre est de cardinal au plus $n$.
  \end{itemize}
\end{remarques}

\begin{notation}
  Par convention, on dira que l'espace vectoriel $\{0_E\}$ est de dimension 0.
\end{notation}

\begin{recherche}
  Déterminez les dimensions des $\bbk$-EV de référence $\bbk^n$, $\bbk_n[X]$ et $\mathfrak{M}_{np}(\bbk)$.
\end{recherche}


\begin{theoreme}[Théorème d'échange de Grassmann]
Soit E un $\bbk$-EV de dimension $n$, soit $\LR=(\ell_1,\ell_2,\dots,\ell_p)$
une famille libre de E et soit $\GR=(g_1,g_2,\dots,g_q)$ une famille génératrice de
E.

Alors:

\begin{itemize}
\item $p \leqslant n \leqslant q$
\item       E        possède       une       base       de        la       forme
  $(\ell_1,\ell_2,\dots,\ell_p,g_{i_1},g_{i_2},\dots,g_{i_{n-p}})$   c'est-à-dire
  qu'on peut  compléter la famille  libre $\LR$ avec  $n-p$ vecteurs de  la famille
  $\GR$ pour former une base de E.
\end{itemize}

\end{theoreme}
\bclampe C'est une  application des corrolaires précédents et du  théorème de la
base incomplète appliqué à la partie libre $\LR$ et la partie génératrice $\LR ∪ \GR$.



\begin{recherche}
  Vérifier que $\LR=(X+1,X+42)$ est libre  dans $E=\bbk_3[X]$ et complétez $\LR$
  pour former une base de E.
\end{recherche}


On en déduit aussi le corrolaire suivant:

\begin{theoreme}[Conditions sur le cardinal pour avoir une base]
  
Soit $E$ un espace vectoriel de dimension finie $ n \in \N^* $. 
\begin{enumerate}
\item Soit $ \mathscr{L} $ une famille libre de $E$. Alors $\mathscr{L}$ est une base de $E$ si, et seulement si, $ \#\mathscr{L} = n $.
%
\item Soit $ \mathscr{G} $ une famille génératrice de $E$. Alors $\mathscr{G}$ est une base de $E$ si, et seulement si, $ \#\mathscr{G} = n $. 
\end{enumerate}
\end{theoreme}


\bclampe Pour le premier point, utiliser  le théorème d'échange. Pour le second,
raisonner par l'absurde en supposant que $\GR$ n'est pas libre.


\begin{recherche}
  Démontrer que $(42, X+74, X^2+12X+37)$ est une base de $ℝ_2[X]$.
\end{recherche}



\subsection{Rang d'une famille finie de vecteurs}



\begin{definition}[Rang d'une famille finie de vecteurs]
Soit $E$ un espace vectoriel et soit $ \( f_1 , f_2 , \dots , f_p \) $ une famille finie de vecteurs de $E$. On appelle \textbf{rang} de la famille $ \( f_1 , f_2 , \dots , f_p \) $ et on note $ \rg \( f_1 , f_2 , \dots , f_p \) $ la dimension du sous-espace vectoriel qu'elle engendre:
%
\[ \rg \( f_1 , f_2 , \dots , f_p \) = \dim \( \Vect \(f_1,f_2, ... , f_p \) \) .\]
\end{definition}


\begin{recherche}
  Déterminer les  rangs des familles de  $\bbk_2[X]$ suivantes: $\FR_1=(X,X^2)$,
  $\FR_2=(1,X,X+1,X^2)$, $\FR_3=(X^2,(X+1)^2)$.
\end{recherche}


\begin{recherche}
 Soit E un $\bbk$-EV de dimension $n∈ℕ^*$. Soit B une partie de E. Démontrer que
 les conditions suivantes sont équivalentes:
 \begin{enumerate}
 \item B est une base de E.
 \item B est libre et $\#B=n$.
 \item B est génératrice et $\#B=n$.
 \end{enumerate}
\end{recherche}




\subsection{Sous-espaces vectoriels d'un espace vectoriel de dimension finie} 

Voici un théorème qui servira surtout à montrer que deux EV sont égaux:

\begin{theoreme}
  Soit $E$ un espace vectoriel de dimension finie et $G$ un sous-espace vectoriel de $E$. Alors $G$ est un espace vectoriel de dimension finie et
%
\[ \dim (G) \leq \dim (E) .\] 
%
De plus, $ \dim (G) = \dim (E) $ si, et seulement si, $ G = E $.
\end{theoreme}

\bclampe On  traite à part  $E=\{0_E\}$. Ensuite  on considère une  famille libre
maximale $\FR=(f_1,\dots,f_m)$ de F qui existe sachant que toute famille de cardinal
$\dim(E)+1=n+1$   est   liée   dans   E    donc   dans   F.   Considérer   alors
$(f_1,\dots,f_m,f)$ avec $f$ n'importe quel élément de F. Que peut-on en déduire
sur $\FR$?

Pour  le 2e  point, raisonner  par l'absurde  pour la  réciproque et  penser aux
dimensions (le cas direct est bien sûr immédiat).


\begin{recherche}
  Déterminer  des  bases  de  $\mathfrak{A}_2(\bbk)$  et  $\mathfrak{S}_2(\bbk)$
  l'ensemble des matrices carrées d'ordre 2 antisymétriques et symétriques.
\end{recherche}


\begin{notation}[Sous-espaces particuliers, cas particuliers]
  Soit $E$ un espace vectoriel de dimension finie $n \geq 2$.
\begin{itemize}
\item Une \textbf{droite vectorielle} de $E$ est un sous-espace vectoriel de $E$ de dimension $1$.
\item Un \textbf{plan vectoriel} de $E$ est un sous-espace vectoriel de $E$ de dimension $2$.
\item Un \textbf{hyperplan vectoriel} de $E$ est un sous-espace vectoriel de $E$ de dimension $n-1$.

\end{itemize}
\end{notation}

\begin{recherche}
  Quels sont les hyperplans de $\bbr^2$? de $\bbr^3$?
\end{recherche}

\section{Sous-espaces supplémentaires}

Dans toute la suite, E est un $\bbk-EV$ non nul de dimension finie.

\subsection{Somme de deux sous-espaces vectoriels}


\begin{definition}[Somme de deux sous-espaces vectoriels]

  Soit  $F$ et $G$ deux sous-espaces
  vectoriels de $E$. L'ensemble $H$ des éléments de $E$ s'écrivant sous la forme
  de  la  somme  d'un  élément  de  $F$  et  d'un  élément  de  $G$  est  appelé
  \textbf{somme des sous-espaces vectoriels} $F$ et $G$. On note: 
%
\[ H = F + G = \left\{ u \in E \mid u = x + y ∧ (x,y) \in F \otimes G \right\} .\]
\end{definition}


\begin{theoreme}[Somme de deux sous-espaces vectoriels]
  $F$ et $G$ étant deux sous-espaces
  vectoriels de $E$, leur somme $F+G$ est un sous-espace vectoriel de E.
  
\end{theoreme}

\bclampe Vérification classique du critère des SEV


\begin{definition}[Somme directe de deux sous-espaces vectoriels]
Soit  $F $ et $ G$ deux sous-espaces vectoriels de $ E$. On dit que la somme $F+G$ est une \textbf{somme directe} lorsque
%
\[ F \cap G = \{ 0_E \} .\]
%
Elle est alors notée $ F \oplus G $.
  
\end{definition}


Comme $F$ et $G$ sont des espaces vectoriels,  on a toujours $ \{ 0 \} \subset F
\cap G  $. Il  suffit donc de  montrer que  $ F \cap  G \subset \{  0 \}  $ pour
montrer qu'une somme est directe.


\begin{recherche}
  Montrer que $ \R ^2 = \Vect( (0,1) ) \oplus \Vect( (1,0) ) $. (2 étapes...)
\end{recherche}


\begin{theoreme}[Unicité de la décomposition dans $F\oplus G$]
  Soit $F$ et $G$ deux sous-espaces vectoriels de $E$. La somme $F+G$ est une somme directe si et seulement si tout élément $u$ de $F+G$ s'écrit de manière unique sous la forme:
%
\[ u = x + y \text{ avec } ( x , y ) \in F \times G .\]
\end{theoreme}

\bclampe $⟹$ : par l'absurde comme d'habitude. $⟸$ : $u∈F∩G ∧ u=0_E+u=u+0_E$.




\subsection{Sous-espaces vectoriels supplémentaires}


\begin{definition}[Sous-espaces vectoriels supplémentaires]
  Soit $F$ et $G$ deux sous-espaces vectoriels de $E$. On dit que $F$ et $G$ sont \textbf{supplémentaires dans E} lorsque $ E = F \oplus G $.
\end{definition}



\begin{recherche}
  Démontrer que $\Vect(X^2)⊕ℝ_1[X]=ℝ_2[X]$ et que $\Vect(42X^2)⊕ℝ_1[X]=ℝ_2[X]$. Remarque?
\end{recherche}



\begin{theoreme}[Existence d'un supplémentaire en dimension finie]
  Tout SEV de E admet au moins un supplémentaire dans E.
\end{theoreme}

\bclampe Traiter d'abord  les cas triviaux: $F=\{0_E\}$ et  $F=E$. Sinon, penser
au théorème d'échange: soit $ (e_1, \dots, e_k) $ une base de $F$. Cette famille
est libre dans  $E$, donc on peut la  compléter en une base $  (e_1, \dots, e_k,
e_{k+1}, \dots, e_n) $ de $E$.  Il reste à démontrer que $ G = \Vect ( (e_{k+1}, \dots,
e_n) ) $ est un supplémentaire de $F$ dans $E$.

\medskip

Établissons un petit lemme qui nous servira pour établir la formule de Grassmann>



\begin{theoreme}[Lemme]
Soit  $F $ et $ G$ deux sous-espaces vectoriels de $ E$. 
  
$$F∩G=\{0_E\} ⟹ \dim(F)+\dim(G)=\dim(F+G)$$
  
\end{theoreme}


\bclampe Introduisez des bases de F et G: démontrer que leur concaténation est une base de $F+G$.

\begin{theoreme}[Formule de Grassmann]

  Soit $F $ et $ G$ deux sous-espaces vectoriels de $ E$.

  
  $$ \dim(F+G) = \dim(F)+\dim(G)-\dim(F∩G)$$
\end{theoreme}

\bclampe Introduisez le supplémentaire $G_1$ de $F∩G$ dans G et démontrez que $F⊕G_1=F+G$.


\begin{theoreme}[Caractérisation de deux sous-espaces vectoriels supplémentaires]
Soit  $F$ et $G$ deux sous-espaces vectoriels de $E$. Les propriétés suivantes sont équivalentes:
\begin{enumerate} 
\item $F$ et $G$ sont deux sous-espaces vectoriels supplémentaires dans $E$,

\item $ F \cap G = \{0_E\} $ et $ \dim (F) + \dim (G) = n $, 

\item $ E = F + G $ et $ \dim (F) + \dim (G) = n $, 

\item Si $B_1$ est une base de $F$ et $B_2$ est une base de $G$, la famille $B$ obtenue en juxtaposant les vecteurs de $B_1$ et ceux de $B_2$ est une base de $E$.
\end{enumerate}
\end{theoreme}

\bclampe $1 ⟹ 2 ⟹ 3 ⟹ 4 ⟹ 1$


\begin{theoreme}[Somme de $k$ sous-espaces vectoriels]
Soit  $k \in [\![ 2 , n ]\!] $. Soit $F_1$, $F_2$, \dots, $F_k$ des sous-espaces vectoriels de $E$. On appelle \textbf{somme des k sous-espaces vectoriels} $F_1$, $F_2$, \dots, $F_k$ et on note $ F = \Sum _{i=1} ^k F_i $ l'ensemble des éléments $ u \in E $ s'écrivant sous la forme $ u = \Sum _{j=1} ^k u_j $, avec pour tout $ j \in [\![ 1 , k ]\!] $, $ u_j \in F_j $. 
\end{theoreme} 


$ F = \Sum _{i=1} ^k F_i $ est un sous-espace vectoriel de $E$.


\begin{definition}[Somme directe de $k$ sous-espaces vectoriels]
Soit $k \in [\![ 2 , n ]\!] $. Soit $F_1$, $F_2$, \dots, $F_k$ des sous-espaces vectoriels de $E$. La somme $ F = \Sum_{i=1} ^{k} F_i $ est une \textbf{somme directe} lorsque tout élément de $F$ s'écrit d'une manière unique sous la forme $ u = \Sum _{j=1} ^k u_j $, avec pour tout $ j \in [\![ 1 , k ]\!] $, $ u_j \in F_j $. On note alors $ \displaystyle F = \bigoplus _{i=1} ^{k} F_i $.
  
\end{definition}

Par exemple $ \bbk^n = \bigoplus _{i=1} ^n \Vect(e_i) $,
%
où les $e_i$ sont les vecteurs de la base canonique.


\begin{theoreme}[Caractérisations de sommes directes en dimension finie]
Soit $E$ un espace vectoriel de dimension finie $n \in \N^*$, et $k \in [\![ 2 , n ]\!] $. Soit $F_1$, $F_2$, \dots, $F_k$ des sous-espaces vectoriels de $E$, munis respectivement des bases $ \mathscr{B}_1 $, $ \mathscr{B}_2 $, \dots , $ \mathscr{B}_k $. On note $ F = \sum _{i=1} ^k F_i $. Les propriétés suivantes sont équivalentes:
\begin{enumerate}
\item $F_1$, $F_2$, \dots, $F_k$ sont en somme directe.
\item $ \forall ( u_1 , u_2 , \dots , u_k ) \in F_1 \times F_2 \times \dots \times F_k , \quad u_1 + u_2 + \dots + u_k = 0 \Longrightarrow  \forall i \in [\![ 1 , k ]\!], u_i = 0 $.

\item La famille obtenue en juxtaposant une base $ \mathscr{B}_1 $ de $ F_1 $, une base $ \mathscr{B}_2 $ de $ F_2 $, \dots et une base $ \mathscr{B}_k $ de $ F_k $ est une base de $F$.

\item $ \dim (F) = \sum _{j=1} ^{k} \dim (F_j) $.
\end{enumerate}
  
\end{theoreme}


\bclampe  On   montre  que  (1)  $\Longrightarrow$   (2)  $\Longrightarrow$  (3)
$\Longrightarrow$   (1),   puis   que   (3)   $\Longrightarrow$   (4)   et   (4)
$\Longrightarrow$ (3).






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                  E X E R C I C E S 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




\modeexercice

\section{Combinaison d'exercices}


\begin{exercice}[Remise en route]
  On travaille dans le $ℝ$-ev $ℝ^{2}$.


\begin{enumerate}
\item $u=\left( 6,-9\right) $\ et $v=\left( -10,15\right) .$\ Donner des CL
de la famille $\left( u\right) ,$\ de la famille $\left( u,v\right) .$

\item Que repr\'{e}sente ${\Vect}\left( \mathscr{F}\right) $\ avec $%
\mathscr{F}=\left( u,v\right) $ ?

\item D\'{e}montrer que $w=\left( 2,-3\right) \in {\Vect}\left( 
\mathscr{F}\right) .$

\item $u$\ et $v$\ sont-ils colin\'{e}aires ?

\item $\mathscr{F}$\ est-elle libre ou li\'{e}e ?

\item D\'{e}montrer que ${\Vect}\left( \mathscr{F}\right) ={\Vect}%
\left( w\right) .$

\item D\'{e}montrer que $\left( 1,2\right) \notin {\Vect}\mathscr{F}.$

\item $t=\left( 1,2\right) ,$\ et $\mathscr{F}^{\prime }=\left( w,t\right) $%
\ ; d\'{e}montrer que ${\Vect}\left( \mathscr{F}^{\prime }\right) =%
\mathbb{R}^{2}.$
\end{enumerate}

  
\end{exercice}



\begin{exercice}
  Démontrer  que toute  sur-famille d'une  famille liée  est liée  et que  toute
  sous-famille d'une famille libre est libre.
\end{exercice}


\begin{exercice}[Vrai ou faux?]
  \begin{enumerate}
  \item Une famille est liée si, et  seulement si chacun de ses éléments s'écrit
    comme combinaison linéaire des autres.
  \item Une famille est libre si, et seulement si, ses éléments sont deux à deux
    non colinéaires.
  \item      Une      famille      $(x_1,\dots,x_n)$      est      liée      si:
    $\ds\sum_{i=1}^nλ_ix_i=0_E ⟹ (λ_1,\dots,λ_n)\neq (0_\bbk,\dots,0_\bbk)$.
  \item Si $\dim(E)=n$ alors toute famille de $n$ éléments est une base.
  \item $\dim(F+G+H)=\dim(F) +\dim(G)+\dim(H)-\dim(F∩G)-\dim(F∩H)-\dim(G∩H)+\dim(F∩G∩H)$
  \end{enumerate}
\end{exercice}


\begin{tcolorbox}
  On retiendra qu'il  est souvent utile de matérialiser un  problème général en
  dimension $n$ en prenant un exemple en dimension 3.

  On fera attention à ne pas  confondre union et somme d'espaces vectoriels. Une
  somme  d'espace  vectoriel  est  l'ensemble  des  combinaisons  linéaires  des
  vecteurs des  deux espaces.  C'est beaucoup  plus grand  qu'une union  qui, en
  règle générale, n'est même pas un espace vectoriel.
  
\end{tcolorbox}

\begin{exercice}[à compléter]
  Compléter,  si possible,  la famille  $\FR$  en une  base  de E  dans les  cas
  suivants:
  \begin{enumerate}
  \item $\FR=\bigl((1,0,-1,0), (1,1,0,0)\bigr)$, $E=ℝ^4$.
  \item $\FR=(1-X^2, 1+X)$, $E=ℝ_4[X]$
  \item $\FR=(X^3-X, X^3+X^2)$, $E=ℂ_3[X]$.
  \item $\FR=\left(
      \left(\begin{smallmatrix}
        1&0\\-1&0
      \end{smallmatrix}\right),
            \left(\begin{smallmatrix}
        1&1\\0&0
      \end{smallmatrix}\right)
\right)$, $E=\mathfrak{M}_2(\bbk)$.
  \end{enumerate}
  
\end{exercice}




\begin{exercice}
  Dire si les familles $\mathscr{F}=\left( u_{i}\right) $ suivantes de $%
  \mathbb{R}^{3}$ sont libres ou li\'{e}es:
  
\begin{enumerate}
\item $u_{1}=(1,1,0),u_{2}=(1,0,2)$

\item $u_{1}=(1,1,0),u_{2}=(1,0,2),u_{3}=(0,1,-2)$

\item $u_{1}=(1,1,0),u_{2}=(1,0,2),u_{3}=(0,1,-1)$

\item $u_{1}=(1,1,0),u_{2}=(1,0,2),u_{3}=(0,1,-1),u_{4}=\left(
12,155,703\right) $
\end{enumerate}
\end{exercice}

\begin{exercice}
  $\mathscr{B}=\left( u_{1},u_{2},u_{3},u_{4}\right) $ est une base de $%
  \mathbb{R}^{4}.$
  
\begin{enumerate}
\item $\mathscr{F}%
_{1}=(u_{1,}u_{1}+u_{2},u_{1}+u_{2}+u_{3},u_{1}+u_{2}+u_{3}+u_{4})$ est-elle
une base de\textit{\ }$\mathbb{R}^{4}?$

\item $\mathscr{F}_{2}=(u_{1}+u_{2},u_{2}+u_{3},u_{3}+u_{4},u_{4}+u_{1})$%
\textit{\ }est-elle une base de\textit{\ }$\mathbb{R}^{4}$\textit{\ ?}
\end{enumerate}

\end{exercice}



\begin{exercice}
  Déterminer en  quoi le calcul  matriciel peut  permettre de déterminer  si une
  famille est une base?
\end{exercice}



\begin{exercice}
  On travaille dans l'ev $\mathbb{R}^{3}$:
  
\begin{enumerate}
\item On note $H=\left\{ \left( x,y,z\right) \in \mathbb{R}^{3}\mid
x+2y+3z=0\right\} ,$\ d\'{e}montrer que $H$\ est un sev de $\mathbb{R}^{3}$
en d\'{e}terminant $u$\ et $v,$\ deux vecteurs de $\mathbb{R}^{3},$\ de
sorte que $H={\Vect}\left( u,v\right) .$

\item Donner une famille g\'{e}n\'{e}ratrice de $H.$

\item La famille $\left( u,v\right) $\ est-elle libre ?

\item Donner une base de $H.$

\item Quelle est la dimension de $H$\ ?

\item On note $w=(1,2,3)$, d\'{e}montrer que $w\notin H.$

\item On note $K={\Vect}(w).$\ Donner une base de $K.$

\item Quelle est la dimension de $K$\ ?

\item D\'{e}montrer que $\left( u,v,w\right) $ est une base de $\mathbb{R}%
^{3}.$
\end{enumerate}
\end{exercice}


\begin{exercice}
  Que penser  de l'ensemble  des suites  géométriques de raison  2 point  de vue
  espace    vectoriel    ?   Des    suites    arithmétiques    ?   Des    suites
  arithmético-géométriques? Des suites récurrentes linéaires d'ordre 2 vérifiant
  $u_{n+2}=-2u_{n+1}+3$?

\end{exercice}


\begin{exercice}
D\'{e}montrer que $H=\left\{ \left( x,y,z\right) \in \mathbb{R}^{3}\mid
x-z=0\right\} $ est un sev de $\mathbb{R}^{3}$ et en donner une base.
\end{exercice}

\begin{exercice}
D\'{e}montrer que $H=\left\{ \left( x,y,z\right) \in \mathbb{R}^{3}\mid
x=0\right\} $ est un sev de $\mathbb{R}^{3}$ et en donner une base.
\end{exercice}


\begin{exercice}
  Soit  $F=\bigl\{(x_1,\dots,x_n)∈\bbk^n\mid   \ds\sum_{k=1}^nx_k=0_{\bbk^n}\bigr\}$  et
  $G=\bigl\{(λ,λ,\dots,λ)∈\bbk^n \bigr\}$.

  \begin{enumerate}
  \item Démontrer que $F⊕G=\bbk^n$.
    À  cette  fin,  on pourra  raisonner  par
    \textbf{analyse-synthèse}:

\begin{tcolorbox}
    \begin{itemize}
    \item Pour  la phase d'\textbf{analyse}, on  suppose avoir une écriture  d'un élément
      $x$ de $\bbk^n$ sous la  forme $y + z$ avec $(y,z) \in F  \times G$ et on tente
      d'obtenir  $y$  et  $z$  en  fonction de  $x$.  Ceci  donne  l'unicité  de
      l'écriture, donc le fait que la somme est directe.
    \item Pour la  phase de \textbf{synthèse}, on  pose $y$ et $z$ comme  obtenus dans la
      phase d'analyse et on montre que l'écriture convient. 
    \end{itemize}
  \end{tcolorbox}

\item Déterminer une base de F et une de G.
  \end{enumerate}
\end{exercice}



\begin{exercice}
  Démontrer que $\mathfrak{S}_n(\bbk) ⊕\mathfrak{A}_n(\bbk)=\mathfrak{M}_n(\bbk)$.
\end{exercice}


\begin{exercice}
  Dans le $ℝ$-EV $ℝ^3$ on donne:
  $$F=\bigl\{(x,y,z)∈ℝ^3\mid 2x-y+z=0\bigr\}\text{ et } G=\bigl\{(x,y,z)∈ℝ^3\mid
  y+z=0\bigr\}$$
  \begin{enumerate}
  \item Vérifier que F et G sont des SEV de $ℝ^3$.
  \item La somme $F+G$ est-elle directe?
  \item Déterminer un SEV supplémentaire de $F∩G$ dans F que l'on notera $F_1$.
  \item Déterminer un SEV supplémentaire de $F∩G$ dans G que l'on notera $G_1$.
  \item Démontrer que $F_1⊕(F∩G)⊕G_1=ℝ^3$.
  \end{enumerate}
\end{exercice}


\begin{exercice}
 Déterminer un supplémentaire dans $ℝ^3$ de $F=\Vect\bigl((0,1,1),(1,0,2)\bigr)$.
\end{exercice}


\begin{exercice}
  On  note $E=ℝ_n[X]$  avec $n  \geqslant 3$.  Soit $F=\bigl\{P  ∈ℝ_n[X]\mid X^3
  \text{       divise      }       P\bigr\}$      et       $G=\Vect\bigl(X(X-1),
  (X-1)(X-2),X(X-2)\bigr)$. Démontrer que $F⊕G=E$.
\end{exercice}

\begin{exercice}
  Soit F et G deux SEV d'un $\bbk$-EV E de dimension finie. On suppose qie $\dim
  F + \dim G > \dim E$. Démontrer que $F∩G\neq \bigl\{0_E\bigr\}$.
\end{exercice}

\begin{exercice}
  Soit $H_1$ et  $H_2$ deux hyperplans distincts d'un  $\bbk$-espace vectoriel E
  de dimension finie.

  En comsidérant  un élément  $a_1∈H_1\setminus H_2$, vérifiez  que $\bbk  a_1 ⊕
  H_2=E$ puis que $H_1+H_2=E$ et démontrer que $\dim(H_1∩H_2)=\dim E-2$. 
\end{exercice}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "PolyEVfinis20"
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
