\chapterimage{taylor}
\chaptertext{\og But the Velocities of the Velocities, the second, third, fourth
  and   fifth  Velocities,   \&c.  exceed,   if  I   mistake  not,   all  Humane
  Underftanding.  The further  the Mind  analyseth and  pursueth these  fugitive
  Ideas,  the more  it  is lost  and bewildered\fg{}  C'est  ainsi que  l'évêque
  George  \textsc{Berkeley}(1685-1753)   s'en  prend   dans  \textit{l'Analyste}
  (1734) aux mathématiciens qui sont prompts  à nier l'existence de Dieu tout en
  accordant foi à des notions qui, pour \textsc{Berkeley}, relèvent tout autant de la
  croyance. À cette  époque, l'ironie de \textsc{Berkeley} n'était pas  si déplacée étant
  donné l'état  d'avancement de  l'Analyse. Elle a  incité les  mathématiciens à
  réfléchir aux  fondements des  mathématiques et  cela les  a occupés  bien des
  siècles.\\ Essayons  de nous faire  notre propre  opinion sur cette  notion de
  dérivées successives  et découvrons comment Brook  \textsc{Taylor}, représenté
  ci-contre, refait
  surface dans nos aventures analytiques.}


\chapter{Dérivées successives (ou le retour de Taylor)}

\section{Qu'est-ce que la deuxième dérivée? Une approche intuitive}


Nous allons dans un premier temps aborder le problème des dérivées successives à
la manière des savants des années 1700.

\subsection{Variation des variations}

Supposons  qu'un fonction  $f$ soit  dérivable  sur un  intervalle I  et que  sa
fonction  dérivée soit  elle-même dérivable  sur ce  même intervalle  (c'est une
fonction donc on peut légitimement étudier sa dérivée Bishop \textsc{Berkeley}).

On  a  fait  le lien  dans  un  précédent  chapitre  entre dérivée  et  sens  de
variation: si  $f'(x)>0$ pour tout  $x$ de I, alors  $f$ est croissante  sur cet
intervalle, si  $f'(x)<0$ pour tout  $x$ de I, alors  $f$ est décroissante  sur cet
intervalle  et  les  réels   vérifiant  $f'(x)=0$  sont  appelés  \textit{points
  stationnaires}.

\begin{recherche}
  Comment appliquer ces notions à $f'$?

  Faites aussi des dessins.
\end{recherche}

\subsection{De conversione functionum in series}

C'est le  titre du 4\eme  chapitre de  la 2\eme partie  du \textit{Institutiones
  calculi differentialis cum eius usu in analysi finitorum ac doctrina serierum}
publié en 1755 par  Leonard \textsc{Euler}



%\begin{figure}
\begin{center}
  \includegraphics[width=0.9\linewidth]{./IMG/conversione}
\end{center}
%\end{figure}

qui utilise  le \textit{Methodus  incrementorum directa  \& inversa}  publié par
Brook \textsc{Taylor} en 1715:


%\begin{figure}
\begin{center}
  \includegraphics[width=0.9\linewidth]{./IMG/incrementorum}
\end{center}
%\end{figure}

Mais en fait on a découvert que Jean \textsc{Bernoulli} avait déjà introduit ses
\textit{series universalissima} en 1694:


%\begin{figure}
\begin{center}
  \includegraphics[width=0.9\linewidth]{./IMG/universalissima}
\end{center}
%\end{figure}


L'idée  de \textsc{Taylor}  et \textsc{Bernoulli}  est d'approcher  une fonction
suffisamment régulière par un polynôme.  Nous allons illustrer cette démarche en
se contentant de l'ordre 2.

On cherche donc un polynôme P de degré 2 qui coinciderait avec une fonction donnée
en trois points.  On dit alors que P est  un polynôme \textit{interpolateur} (du
latin \textit{interpolatio}:  altération, action de  changer ça et là).  Il sera
interessant de comparer avec l'étymologie du mot \textit{extrapolation} qui a été
formé par  l'Académie des  Sciences en  1877 par  opposition à  interpolation en
changeant   le   préfixe   \textit{inter}(entre...)   par   \textit{extra}   (en
dehors...).  Un  peu  comme  \textit{cis}genre   a  été  créé  en  opposition  à
\textit{trans}genre. Comme quoi le latin, ça peut servir.

\medskip

On  cherche  alors par  exemple  un  polynôme P  de  degré  2 qui  s'écrira  $P=
aX^2+bX+c$ et qui vérifiera $P(0)=f(0)$,
$P(0+h)=f(0+h)$, $P(0+h+h)=f(0+h+h)$.

Déterminer P c'est donc trouver $a$, $b$ et $c$.

\begin{recherche}
  \begin{enumerate}
  \item   Démontrer   que  $f(0)=c$,   $f(h)=f(0)   +   bh   +  ah^2$   et   que
    $f(2h)=2f(h)-f(0)+2ah^2$.
  \item Calculer $\ds\lim_{h\to 0}\dfrac{\dfrac{f(0+h+h)-f(0+h)}{h}-\dfrac{f(0+h)-f(0)}{h}}{h}$.\\  En
    déduire les valeurs de $a$ et $b$ en fonction de $f''(0)$ et $f'(0)$.
  \item Écrire alors $P(x)$ en fonction de $x$, $f(0)$, $f'(0)$ et $f''(0)$.
  \item À quoi correspond la partie de degré 1 du polynôme interpolateur?
  \item Illustrer par des exemples en Scilab la similarité de $P(x)$ avec $f(x)$.
  \end{enumerate}
\end{recherche}

Voici par exemple ce que cela donne pour $x\mapsto \ln(1+x)$:

%\begin{figure}
\begin{center}
  \includegraphics[width=\linewidth]{taylorln}
\end{center}
% \end{figure}


Mais tout ceci reste un peu empirique. Il est temps de mettre un peu d'ordre.


\section{Dérivées successives}

\subsection{La classe}

On connait déjà les fonctions de classe $C^1$. Voici leurs cousines.

\begin{definition}[Classe $C^2$]
On dit que $f$ est deux fois dérivable sur $I$ lorsque $f$ est de classe $C^1$ sur $I$ et que $f'$ est dérivable sur $I$. On note alors $ (f')' = f^{(2)} $.

On dit que $f$ est \textbf{de classe $C^2$} sur $I$ lorsque $f$ est deux fois dérivable sur $I$ et que $f^{(2)}$ est continue sur $I$. On note alors $ f \in C^2(I,\R) $.
\end{definition}


\begin{definition}[Classe $C^p$]
On dit que $f$ est $p$ fois dérivable sur $I$ lorsque $f$ est de classe $C^{p-1}$ sur $I$ et que $f^{(p-1)}$ est dérivable sur $I$. On note alors $ (f^{(p-1)})' = f^{(p)} $.

On dit que $f$ est \textbf{de classe $C^{(p)}$} sur $I$ lorsque $f$ est $p$ fois dérivable sur $I$ et que $f^{(p)}$ est continue sur $I$. On note alors $ f \in C^p(I,\R) $.
\end{definition}



\begin{definition}[Classe $C^\infty$]
On dit que $f$ est \textbf{de classe $C^\infty$} sur $I$ lorsque $f$ est indéfiniment dérivable, c'est à dire dérivable à tout ordre. On note alors $ f \in C^\infty (I,\R) $.
\end{definition}


\begin{notation}
  Si $f$ est continue sur $I$, on notera par convention $ f \in C^0 (I,\R) $ et $ f^{(0)} = f $.
\end{notation}


\subsection{Un nouveau tableau}


...à compléter!


\begin{center}
\begin{tabular}{|c|c|c|}
	\hline
	     $f(x)$      & $D_{f'}$ 
	   				 & $f^{(n)}(x)$           \\ \hline
% % % % % % % % % % % % % % % % % % % % % % % % % % % %
	   				 	                 &&\\
	   				 	     $e^x$       & \Trou{$\R$} 
	   				 				     & \Trou{$e^x$}               \\
% % % % % % % % % % % % % % % % % % % % % % % % % % % %
	                 &&\\
$x^p$ ($p \in \N^*$) & \Trou{$\R$} 
					 & \Trou{ $\begin{cases*}
					 \dfrac{p!}{(p-n)!}x^{p-n} & si $n \leq p$ \\
					 0 & si $ n>p $
					 \end{cases*}$ } \\ 
% % % % % % % % % % % % % % % % % % % % % % % % % % % %
	                 &&\\
$x^\alpha$ ($\alpha \in \R \setminus \N$) 
					 & \Trou{$\R_+ ^*$} 
					 & \Trou{$ \alpha(\alpha-1) \ldots (\alpha-n+1) x^{\alpha-n} $ }\\
					 && \Trou{ $ = \( \ds\prod_{i=0}^{n-1}(\alpha-i)\) x^{\alpha-n}$}  \\
% % % % % % % % % % % % % % % % % % % % % % % % % % % %

  % % % % % % % % % % % % % % % % % % % % % % % % % % % %
	                 &&\\
	    $\cos x$     & \Trou{$\R$} 
				     & \Trou{$\cos\(x+n\dfrac{\pi}{2}\)$}    \\
% % % % % % % % % % % % % % % % % % % % % % % % % % % %
	                 &&\\
	    $\sin x$     & \Trou{$\R$} 
	  				 & \Trou{$\sin\(x+n\dfrac{\pi}{2}\)$}    \\
% % % % % % % % % % % % % % % % % % % % % % % % % % % %
	                 &&\\
	$\dfrac{1}{a+x}$ & \Trou{$ \R \setminus \{ -a \} $} 
					 & \Trou{$\dfrac{(-1)^n n!}{(a+x)^{n+1}}$} \\
% % % % % % % % % % % % % % % % % % % % % % % % % % % %
	                 &&\\
	$\dfrac{1}{a-x}$ & \Trou{$ \R \setminus \{ a \} $} 
					 & \Trou{$\dfrac{ n!}{(a-x)^{n+1}}$}    \\
	                 &&\\ \hline
\end{tabular}
\end{center}


\section{Opérations algébriques}

\subsection{Linéarité}


\begin{theoreme}
  Soit $p \in \N$, alors $ C^p (I,\R) $ est un espace vectoriel.
\end{theoreme}



\bclampe C'est un sous-espace vectoriel d'un des cinq doigts de la main. Pour la
stabilité, on  procède par récurrence  sur $p$ en  utilisant la linéarité  de la
dérivation.

\begin{remarque}
  On en déduit donc que $ C^\infty (I,\R) $ a aussi une structure d'espace vectoriel.
\end{remarque}


\subsection{Formule de Leibniz}

\begin{theoreme}[Formule de Leibniz]\label{th::leibniz}
Soit $n \in \N$ et soient $f$ et $g$ des fonctions de classe $C^n$ sur l'intervalle $I$. Alors la fonction $fg$ est de classe $C^n$ sur $I$ et
% 
\[ \(fg\)^{(n)} = \sum_{k=0}^{n} \binom{n}{k} f^{(k)} g^{(n-k)} .\]
\end{theoreme}


\bclampe  Récurrence  sur  $n$.  Ne  négligez  pas  le  \og  passage  en  classe
supérieure\fg{}. Pensez à la formule de \textsc{Pascal}.

\begin{proof}
\GrosTrou{
Soit $ n \in \N $, on pose $P(n)$ la proposition \og{}soient $ (f,g) \in \left(C^n(I)\right) ^2 $, alors $fg \in C^n(I) $ et $ (fg)^{(n)} = \ds\sum_{k=0}^{n} \binom{n}{k} f^{(k)} g^{(n-k)} $\fg{}.
\begin{itemize}
\item Soit $n=0$, le produit de deux fonctions continues sur $I$ est une fonction continue sur $I$, et $ (fg)^{(0)} = f g = \ds\sum_{k=0}^{0} \binom{0}{k} f^{(k)} g^{(0-k)} $. Donc $P(0)$ est vraie.
\item Soit $ n  \in \N $ et soient $f$ et $g$  des fonctions de classe $C^{n+1}$
  sur $I$.
  Alors $f$ et $g$ sont aussi de classe $C^n$ donc $P(n)⟹ fg$ est de classe $C^n$, et:
%
\[P(n)⟹ \(fg\)^{(n)} = \ds\sum_{k=0}^{n} \binom{n}{k} f^{(k)} g^{(n-k)} \]
%
Pour tout $ k \in [\![ 0 , n ]\!] $, $ f^{(k)} $ est de classe $ C^{n+1-k} $, et donc au moins de classe $C^1$. De même, $g^{(n-k)}$ est de classe $ C^{n+1-n+k} $, et donc au moins de classe $C^1$. Donc par produit et somme de fonctions de classe $C^1$, $ \(fg\)^{(n)} $ est de classe $C^1$. Ce qui signifie que $ fg $ est de classe $C^{n+1}$. On obtient alors en dérivant la relation précédente:
%
\begin{align*}
P(n)⟹\(fg\)^{(n+1)}
%
& = \ds\sum_{k=0}^{n} \binom{n}{k} \( f^{(k+1)} g^{(n-k)} + f^{(k)} g^{(n+1-k)} \) \\
%
& = \ds\sum_{k=0}^{n} \binom{n}{k}  f^{(k+1)} g^{(n-k)} + \ds\sum_{k=0}^{n} \binom{n}{k} f^{(k)} g^{(n+1-k)} \\
%
& = \ds\sum_{k=1}^{n+1} \binom{n}{k-1} f^{(k)} g^{(n+1-k)} + \ds\sum_{k=0}^{n} \binom{n}{k} f^{(k)} g^{(n+1-k)} \\
%
& = \binom{n}{0} f g^{(n+1)} + \ds\sum_{k=1}^{n} \( \binom{n}{k-1} + \binom{n}{k} \) f^{(k)} g^{(n+1-k)} + \binom{n+1}{n+1} g f^{(n+1)} \\
%
& = \ds\sum_{k=0}^{n+1} \binom{n+1}{k} f^{(k)} g^{(n+1-k)} \text{ par la formule de Pascal}
\end{align*}
%
Donc $P(n)⟹P(n+1)$, ce qui termine la preuve.
\end{itemize}
}
\end{proof}


\begin{recherche}
  Calculer la dérivée $n$\eme de $f: x\mapsto (x^2+1)\E^{2x}$.
\end{recherche}

\begin{recherche}
  Soit $φ_n:x\mapsto x^{n-1}\ln x$ pour tout entier naturel non nul $n$.

  \begin{enumerate}
  \item Calculer $φ_n^{(n)}(x)$ pour tout $n∈\bigl\{1,2,3\bigr\}$.
    \item  En  déduire  une  formule  générale  donnant  $φ_n^{(n)}(x)$  que  vous
      démontrerez par récurrence.
  \end{enumerate}
\end{recherche}


\subsection{Théorème de composition}

\begin{theoreme}[Théorème de composition]
  Soit $f$  et $g$  deux fonctions  de classe  $C^n$ sur  deux intervalles  I et
  J respectivement, avec $f(I)\subseteq J$.

  Alors $g\circ f$ est de classe $C^n$ sur I.
  
\end{theoreme}


\bclampe   Récurrence   sur   $n$,    dérivée   d'une   composée   et   théorème
\vref{th::leibniz}.



\section{Formules de Taylor et ses amis}



Nous allons voir cette  année un certain nombre de formules  dites de Taylor. La
première qui nous servira de référence  découle de notre capacité à intégrer par
parties. Dans toutes ces formules, vous veillerez bien à vérifier les hypothèses
des théorèmes.

\subsection{Formule de Taylor avec reste intégral}


\begin{theoreme}[Formule de Taylor avec reste intégral (à l'ordre $n$)]
Soit $n \in \N$ et $f$ une fonction de classe $C^{n+1} $ sur l'intervalle $I$. Pour tout $ (a,x) \in I^2 $,
%
\[ f(x) = \sum_{k=0}^{n} \frac{(x-a)^k}{k!} f^{(k)}(a) + \int_{a}^{x} \frac{(x-t)^n}{n!} f^{(n+1)}(t) dt .\]
%
On appelle $ \ds\sum_{k=0}^{n} \frac{(x-a)^k}{k!} f^{(k)}(a) $ la \textbf{partie polynomiale} de la formule, et $ \ds\int_{a}^{x} \frac{(x-t)^n}{n!} f^{(n+1)}(t) dt $ le \textbf{reste intégral d'ordre $n$}.
  
\end{theoreme}

\bclampe Récurrence sur $n$ et intégration par parties du reste d'ordre $n$.




\begin{proof}
\GrosTrou{(démonstration à connaître)
Soit $n \in \N$, et $ (a,x) \in I^2 $. On pose:\\
$P(n) =$ \og{}si $f \in C^{n+1} (I)$, alors $ f(x) = \sum_{k=0}^{n} \frac{(x-a)^k}{k!} f^{(k)}(a) + \int_{a}^{x} \frac{(x-t)^n}{n!} f^{(n+1)}(t) dt $\fg{}.
\begin{itemize}
\item Pour $n=0$, soit $f$ une fonction de classe $C^1$ sur $I$, alors $f'$ est continue, de primitive $f$, et on a:
%
\[ \int _a ^x f'(t) dt = f(x) - f(a) ,\]
%
d'où: $ f(x) = f(a) + \int _a ^x f'(t) dt $. Donc $P(0)$ est vraie.
\item Soit $ n \in \N $.  Soit $f$ une fonction de classe $ C^{n+2} $ sur $I$. Elle est donc aussi en particulier de classe $C^{n+1}$ et:
%
\[P(n)⟹ f(x) = \sum_{k=0}^{n} \frac{(x-a)^k}{k!} f^{(k)}(a) + \int_{a}^{x} \frac{(x-t)^n}{n!} f^{(n+1)}(t) dt .\]
%
Comme $f$ est de classe $ C^{n+2} $ sur  $I$, $ f ^{(n+1)} $ est de classe $ C^1
$ sur  $I$. Or $ t  \to \frac{ -  (x-t) ^{n+1} }{ (n+1)  ! } $ est  également de
classe $C^1$ sur  $I$, on peut donc transformer l'intégrale  par intégration par
parties: 
%
\begin{align*}
\int_{a}^{x} \frac{(x-t)^n}{n!} f^{(n+1)}(t) dt 
& = \left[ \frac{ - (x-t) ^{n+1} }{ (n+1) ! } f^{(n+1)}(t) \right] _a ^x - \int_{a}^{x} \frac{ - (x-t) ^{n+1} }{ (n+1) ! } f^{(n+2)}(t) dt \\
& = \frac{ (x-a) ^{n+1} }{ (n+1) ! } f^{(n+1)}(a) + \int_{a}^{x} \frac{ (x-t) ^{n+1} }{ (n+1) ! } f^{(n+2)}(t) dt .
\end{align*}
%
En remplaçant dans la relation précédente, on obtient
%
\[ P(n)⟹f(x) = \sum_{k=0}^{n} \frac{(x-a)^k}{k!} f^{(k)}(a) + \frac{ (x-a) ^{n+1} }{ (n+1) ! } f^{(n+1)}(a) + \int_{a}^{x} \frac{ (x-t) ^{n+1} }{ (n+1) ! } f^{(n+2)}(t) dt ,\]
%
donc $P(n+1)$ est vraie. Ce qui termine la preuve.
\end{itemize}
}
\end{proof}



\begin{recherche}
  Démontrer la formule de Taylor pour les polynômes en utilisant la formule de Taylor avec reste intégral.
\end{recherche}




\subsection{Inégalité de Taylor-Lagrange}

\begin{theoreme}[Inégalité de Taylor-Lagrange (à l'ordre $n$) version 1]
Soit $n \in \N$ et soit $f$ une fonction de classe $C^{n+1}$ sur un intervalle $I$ de $\R$. On suppose qu'il existe $m$ et $M$ deux réels tels que $\forall t \in I $, $ m \leq f^{(n+1)}(t) \leq M $. Alors $\forall (a,x) \in I^2 $ tels que $a \leq x$,
%
\[ \frac{(x-a)^{n+1}}{(n+1)!} m \leq f(x)-\sum_{k=0}^{n} \frac{(x-a)^k}{k!}f^{(k)}(a) \leq \frac{(x-a)^{n+1}}{(n+1)!} M .\]
\end{theoreme}

\begin{DANGER}
  On a besoin que $a$ soit inférieur à $x$...
\end{DANGER}

\bclampe Formule de Taylor avec reste intégral, utilisation de l'hypothèse et produit par $\frac{(x-a)^n}{n!}$
(qui est bien positif !!!) puis encadrement du reste et
croissance de l'intégrale (car $a \leqslant x$).


\begin{theoreme}[Inégalité de Taylor-Lagrange (à l'ordre $n$) version 2]
Soit $n \in \N$ et soit $f$ une fonction de classe $C^{n+1}$ sur un intervalle $I$ de $\R$. On suppose qu'il existe $ K \in \R $ tel que $ \forall t \in I $, $ \abs{ f^{(n+1)}(t) } \leq K $. Alors $\forall (a,x) \in I^2$,
%
\[ \abs{ f(x) - \sum_{k=0}^{n} \frac{(x-a)^k}{k!} f^{(k)}(a) } \leq \frac{ \abs{x-a} ^{n+1} }{(n+1)!} K .\] 
\end{theoreme}


\begin{remarque}
  Ici, on n'a pas besoin  d'avoir forcément $a \leqslant x$.
\end{remarque}


\bclampe  On utilise  la  version 1  avec  $m=-K$ et  $M=K$ dans  le  cas où  $a
\leqslant x$.
Dans  l'autre cas,  on applique  la  formule de  Taylor avec  reste intégral  en
prenant la valeur  absolue, en inversant les bornes et  en utilisant l'inégalité
triangulaire sur le reste.


\subsection{Un exemple classique: calcul de la limite de la série harmonique alternée}

Soit $ f : x \longrightarrow \ln (1+x) $ définie sur $[0,+\infty[$. 
Étudions la suite $(S_n)$ définie pour $ n \geq 1 $ par :
%
\[ S_n = \sum_{k=1}^{n} \frac{(-1)^{k-1}}{k} .\]

\medskip
La fonction $ f : x \longrightarrow \ln (1+x) $ est de classe $C^\infty$ sur $[0,+\infty[$ et on a: $\forall k \in \N^* $, $ \forall x > 0 $,
%
\[ f^{(k)}(x) = \frac{(k-1)! (-1)^{k-1}}{(x+1)^k}, \text{ donc } \abs{f^{(k)}(x)} = \frac{(k-1)!}{(x+1)^k} \leq (k-1)! .\]
%
On applique l'inégalité de Taylor-Lagrange à l'ordre $n$ entre $0$ et $x > 0$, en majorant $\abs{ f^{(n+1)}(x) }$ par $n!$:
%
\[ \abs{ \ln(1+x) - \sum_{k=1}^{n} \frac{(-1)^{k-1} x^k}{k} } \leq n! \frac{ x ^{n+1} }{ (n+1)! } = \frac{ x ^{n+1} }{ (n+1) } .\]
%
On  trouve  en  particulier  que  pour  $x=1$: $  \abs{  \ln(2)  -  S_n  }  \leq
\frac{ 1 }{ (n+1) } $. Or $ \ds\lim_{n \to +\infty} \frac{ 1 }{ (n+1) } = 0 $. Donc
par encadrement $(S_n)$ converge vers $\ln(2)$.


\begin{remarque}
  Il  existe une  formule de  Taylor-Lagrange dont  la démonstration  utilise le
  théorème  de Rolle.  Nous la  verrons en  exercice. Elle  permet d'énoncer  la
  Formule  de Taylor-Mac  Laurin qui  est un  cas particulier.  Et nous  verrons
  bientôt la formule de Taylor-Young. 
\end{remarque}









%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                  E X E R C I C E S 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\modeexercice


\section{Jouons avec Wilhelm et Brook}

\begin{exercice}
  Calculer la dérivée $n$\eme de  $f:x\mapsto (x^2-x+1)\E^{-x}$ pour tout entier
  naturel $n$.
\end{exercice}


\begin{exercice}
  Soit un entier naturel $n$. Calculer la dérivée $n$\eme de $f:x\mapsto \cos^3(x)$.
\end{exercice}

\begin{exercice}
  Pour  $α∈ℂ$,   calculer  la  dérivée   $q$\eme  de  la   fonction  $f:x\mapsto
  \E^{αx}$. Comment  retrouver les formules  des dérivées $q$\eme  des fonctions
  $\cos$ et $\sin$?
\end{exercice}


\begin{exercice}
  En  appliquant  la  formule  de  Taylor avec  reste  intégral  à  la  fonction
  $\arctan$, démontrer que pour tout réel strictement positif $x$, $\arctan(x)<x$.
\end{exercice}


\begin{exercice}
  Démontrer  que   pour  tout  $x∈ℝ^*_+$   et  pour  tout  $n∈ℕ$,   $\exp(x)  >
  \ds\sum_{k=0}^n \frac{x^k}{k!}$.

  \begin{Aide}
    Taylor reste intégral
  \end{Aide}
\end{exercice}



\begin{exercice}
  Démontrer                                                                  que
  $\ds\lim_{n\to+\infty}\ds\sum_{k=1}^n\frac{(-1)^{k-1}}{k}=\ln(2)$.

  \begin{Aide}
    Taylor-Lagrange et $x\mapsto \ln(1+x)$.
  \end{Aide}
\end{exercice}


\begin{exercice}
Soit              $x\in              ℝ$.              Démontrer              que
$\ds\lim_{n\to+\infty}\ds\sum_{k=0}^n\frac{x^k}{k!}=\E^x$

\begin{Aide}
  Taylor-Lagrange
\end{Aide}
\end{exercice}


\begin{exercice}[Formule de Taylor-Lagrange]

  
  Soit $f$ une fonction de classe $C^{n+1}$ sur un segment $[a,b]$ avec $a\neq b$.  On voudrait
  démontrer qu'il
  existe un réel $c∈]a,b[$ tel que:

  $$f(b)=\sum_{k=0}^n
  \frac{(b-a)^k}{k!}f^{(k)}(a)+\frac{(b-a)^{n+1}}{(n+1)!}f^{(n+1)}(c)$$

  \begin{enumerate}
  \item Étant donné un réel A, considérez la fonction $g:x\mapsto\ds\sum_{k=0}^n
  \frac{(b-x)^k}{k!}f^{(k)}(x)+\frac{(b-x)^{n+1}}{(n+1)!}A   $.   Vérifiez   que
  $g(b)=f(b)$.
  \item Calculer $g'(x)$.
  \item Soit $A$ tel que $g(a)=f(b)$. Ce nombre existe-t-il?
  \item  En  déduire  l'existence  d'un  réel $c∈]a,b[$  tel  que  $A=f^{(n+1)}(c)$  en
    utilisant le théorème de Rolle.
  \end{enumerate}
\end{exercice}



\begin{exercice}[Oral ESCP]

  On considère la fonction $f$ définie sur $ℝ$ par $f(x)=\frac{1}{x^2+1}$.
  \begin{enumerate}
  \item Démontrer que $f$ est de classe $C^\infty$ sur $ℝ$.
  \item Soit un entier naturel $n$. On pose $P_n(x)=(1+x^2)^{n+1}f^{(n)}(x)$.
    \begin{enumerate}
    \item     Démontrer    que     l'on     a,    pour     tout    réel     $x$,
      $(1+x^2)P'_n(x)=2(n+1)xP_n(x)+P_{n+1}(x)$.
    \item Établir que  $P_n$ est un polynôme  de degré $n$ dont  on précisera le
      coefficient dominant.
    \end{enumerate}
    \item Démontrer par récurrence sur $n$ que $P_n$ admet $n$ racines réelles distinctes.
  \end{enumerate}
  
\end{exercice}





\modenormal
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "PolyTaylor"
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
