# Préparer la rentrée de maths en ECS1

Bienvenue en Prépa ! Cette rentrée sera spéciale pour vous à plus d'un titre, notamment après une fin de Terminale pour le moins hors du commun. Il est alors d'autant plus important de bien préparer sa rentrée en Prépa :)

## Communication

Nous allons nous voir une dizaine d'heures par semaine mais cela ne sera pas suffisant ! Vous pourrez poser des questions en pleine nuit, le week-end, quand vous voulez sur le serveur **Riot** de la classe et récupérer les cours, exercices, énoncés sur la forge **Gitlab** de la classe.

#### Serveur Riot

Vous allez créer un compte sur [element](https://element.io/blog/welcome-to-element/) et je vous inviterai à participer à la communauté des ECS1 de l'Externat. `Element` ressemble à `Discord` si certains connaissent, mais en version *open-source* et libéré de la surcouche tentant d'exploiter vos données personnelles. Ainsi nous pourrons converser en groupe avant septembre si certains ont des interrogations durant leurs révisions/approfondissements. Il suffira de [me joindre](mailto:gconnan@uco.fr) en me donnant votre identifiant et je vous enverrai une invitation à rejoindre notre groupe.

#### Gitlab 

Gitlab vous permettra d'avoir accès à une version toujours à jour du cours. Nous apprendrons petit à petit à approfondir son utilisation. Certains curieux peuvent se lancer dans l'exploration de [ce *tutoriel*](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)...en Anglais bien sûr.

## Activités estivales

Mais avant de se lancer, je vous propose quelques activités diverses afin d'arriver en pleine forme début septembre pour attaquer une année riche d'aventures. 

#### Lectures

1. [Un cours de Terminale](http://download.tuxfamily.org/tehessinmath/les%20pdf/PolyTs200910.pdf) à explorer de fond en comble (sauf le chapitre 13). Il vous introduira de manière approfondie à une vision plus large des mathématiques. Ce polycopié correspond au programme d'avant 2012 qui avait vu la plus grande coupe dans les exigences depuis des décennies.
2. [Un pamphlet](https://www.maa.org/sites/default/files/pdf/devlin/LockhartsLament.pdf) (en anglais) sur ce à quoi ressemble trop souvent l'enseignement des mathématiques dans le secondaire et ce qu'il pourrait être. Cela est d'actualité pour nous car vous allez être sollicités de toute part par des méthodes-miracle où une mémorisation aveugle tend à remplacer une réflexion enrichissante. Ce texte introduit également l'aspect esthétique des mathématiques. Bref de quoi alimenter la curiosité intellectuelle qui caractérise les étudiants d'ECS.
3. [Un magnifique livre](https://www.dunod.com/prepas-concours/maths-et-informatique-visa-pour-prepa-2018-2019-mpsi-pcsi-ptsi-bcpst-ecs) :) dans l'esprit du poly de cours mais avec les solutions et une initiation à [Scilab](https://www.scilab.org/) (qui est le logiciel au programme).
4. [Un livre-méthode](https://www.dunod.com/prepas-concours/maths-ecs-ece-ect-visa-pour-prepa) qui complète le précédent avec une vision plus scolaire et par méthode-miracle.
5. [Flatland](https://fr.wikipedia.org/wiki/Flatland), un conte politico-mathématique de la fin du XIXe siècle ([texte original en anglais](https://github.com/Ivesvdf/flatland/blob/master/oneside_a4.pdf?raw=true)) qui vous fera réfléchir et améliorera votre niveau d'anglais. Il existe une [adaptation](https://www.youtube.com/watch?v=eyuNrm4VK2w) sous forme de film d'animation.
6. [Weapons of Math Destruction](https://en.wikipedia.org/wiki/Weapons_of_Math_Destruction) pour réfléchir un peu à l'impact des mathématiques dans le monde de la finance, de léconomie, du marketing,...

#### Films, podcasts, sites

1. [Comment j'ai detesté les maths](https://youtu.be/ar5EEKg3k4s), un film qui donne une vision peut-être nouvelle des mathématiques, de leur enseignement, de leur utilisation en économie : de bonnes pistes de réflexion.
2. [More or less](https://www.bbc.co.uk/programmes/b006qshd), une émission hebdomadaire de la BBC sur les nombres et les statistiques dans le débat politique, l'information et la vie de tous les jours. Des podcasts d'une demi-heure à explorer sans modération.
3. Les conférences d'[Hannah Fry](https://www.hannahfry.co.uk/) notamment les [Christmas Lectures](https://www.rigb.org/christmas-lectures/2019-secrets-and-lies) de décembre 2019 sur *the hidden power of maths*...
4. Le site [images des mathématiques](https://images.math.cnrs.fr/) où les chercheurs du CNRS rendent accessible l'actualité et éclairent des domainent qui intéressent les ECS : économie, politique, histoire, philosophie,...
