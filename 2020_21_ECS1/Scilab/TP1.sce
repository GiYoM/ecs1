
////////////////////////
// Recherche 4.19
/////////////////////////

Besoins = [4 5 2 3; 5 1 3 2; 1 5 2 1];
Offres = [1.2 1.4; 2.1 1.8; 1.3 1.5; 1.6 1.5];
Couts = Besoins*Offres


// avec sum
Total_Pommes = sum(Besoins(:, 1))
Total_Noix = sum(Besoins(:, 3))
Total_MegaU = sum(Besoins(1, :))

// avec for

t_U = 0 // initialisation de la somme
nb_cols = size(Besoins)(2) 
for no_col = 1:nb_cols do
    t_U = t_U + Besoins(1, no_col);
end
//disp(t_U == Total_MegaU)

// fonction qui calcule la sosmme des 
// coeffs de la i-e ligne de la matrice mat
function s = somme_ligne(mat, i)
    s = 0
    nb_cols = size(mat)(2)
    for j = 1:nb_cols do
        s = s + mat(i, j)
    end
endfunction


//////////////////////////////////////////
// Création matrices nulles/identités
// i.e. on fabrique à la main zeros et eye
///////////////////////////////////////////

function z = nulle_nulle(nb_l, nb_c)
    z = []
    for i = 1:nb_l do
        ligne = []
        for j = 1:nb_c do
            ligne = [ligne, 0]
        end
        z = [z ; ligne]
    end
endfunction

function z = nulle(nb_l, nb_c)
    ligne = []
    for j = 1:nb_c do
        ligne = [ligne, 0]
    end
    z = []
    for j = 1:nb_l do
        z = [z; ligne]
    end
endfunction

function idn = identite(n)
    idn = []
    for i = 1:n do
        ligne = []
        for j = 1:n do
            if j == i then // si sur la diagonale
                ligne = [ligne, 1] // on met un 1
            else
                ligne = [ligne, 0] // sinon un 0
            end
        end
        idn = [idn ; ligne] // on ajoute la ligne
    end
endfunction



///////////////////////////////////////////
// Ex 4.31 : création d'une matrice à partir
// du terme général
/////////////////////////////////////////////

function m = mat31_1(n)
    m = []
    for i = 1:n do
        ligne = []
        for j = 1:n do
            if j < i then 
                ligne = [ligne, i + j]
            else 
                ligne = [ligne, 0]
            end
        end
        m = [m ; ligne]
    end
endfunction


function m = mat31_2(n)
    m = []
    for i = 1:n do
        ligne = []
        for j = 1:n do
            if j < i then 
                ligne = [ligne, i]
            else 
                ligne = [ligne, j]
            end
        end
        m = [m ; ligne]
    end
endfunction



//////////////////////////////////////////
//
// Ex 4.36 : test matrice stochastique
//
////////////////////////////////////////////
// renvoie T si m est stochastique, F sinon

function test = stoch(m)
    n = size(m)(1) // nb de lignes
    coeff_pos = %T 
    som_ligne_1 = %T
    i = 1 // on commence ligne 1
    while coeff_pos & som_ligne_1 & i <= n do        
            som = 0 // on initialise la somme de cette ligne
            for j = 1:n do // on parcourt la ligne i
                coeff_pos = (coeff_pos & m(i, j) >= 0)// coeff positif ?
                som = som + m(i, j)
            end
            som_ligne_1 = som == 1
            i = i + 1 // on va voir la ligne suivante
    end    
    test = coeff_pos & som_ligne_1
endfunction
