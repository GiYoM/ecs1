# SCILAB ET LES POLYNÔMES 

## POLYNÔMES FORMELS

On  représentera  dans cette  section  les  polynômes  par  la liste  des  leurs
coefficients en commençant par le coefficient du terme de plus bas degré.

Par exemple $`P=X^4-5X+3`$ sera représenté par `P = [3 -5 0 0 1]`.

1.   Comment  obtenir  le degré  d'un  tel  polynôme  (Attention  ! Il  y  a  un
    piège...) ?  
	
```scilab
--> P = [3 -5 0 0 1];

--> deg(P)
 ans  =

   4.
```
	
   Comment  additionner deux  polynômes? Peut-il  y avoir  un problème?  Comment
   additionner P et $`-2X^2+7`$ ? 
   Créez une fonction `somme(P1, P2)` qui effectue la somme de deux polynômes.

```scilab
--> Q = [7 0 -2];

--> somme(P, Q)
 ans  =

   10.  -5.  -2.   0.   1.
```


2. Créez une fonction qui effectue le produit de deux polynômes.

```scilab
--> R = [1 3];

--> prod(R, R)
 ans  =

   1.   6.   9.
```

3. Créez une fonction qui renvoie le polynôme dérivé.

```scilab

--> R = [1 1 1 1];

--> deriv(R)
 ans  =

   1.   2.   3.
```

4.  Créez une fonction qui calcule l'image d'un réel *x* par la fonction
   polynomiale associée.

```scilab

--> R = [1 2 1];

--> eval(R, 10)
 ans  =

   121.
```

## RACINES DICHOTOMIQUES

On voudrait déterminer les racines d'un polynôme...

5.  Étudiez  la   fonction  polynomiale  associée  au   polynôme  *P*  introduit
   précédemment. Déterminez en particulier le nombre de ses racines.

6. Tracez la représentation graphique de  *P* à l'aide de Scilab pour visualiser
   ces racines. 

7.  Zoomez maintenant  sur  l'intervalle  [-1, 2].  Créez  ensuite une  fonction
   `racine_dicho(P, a, b, prec)` pour  déterminer une approximation de la racine
   d'un polynôme P sur l'intervalle [a, b] avec la précision `prec` en utilisant
   une recherche **dichotomique**.
   

   
8. Modifiez le programme précédent pour  obtenir le nombre d'itérations de votre
   programme.
   
## LES OUTILS SCILAB

9. Explorez  la fonction `poly`  de Scilab,  la fonction `derivat`,  la fonction
   `horner` et réécrivez votre fonction `racine_dicho` avec ces nouveaux outils.
   
   

