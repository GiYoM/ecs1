# TP0 : Découverte de Scilab

## Avant de commencer


Scilab  est un  logiciel libre,  multiplateforme  de calcul
numérique orienté  informatique industrielle  et ingénierie  (bref, parfaitement
adapté aux ECS).



SCILAB  est téléchargeable  rapidement (quand  on n'a  pas de  Mac) à  l'[adresse
suivante](https://www.scilab.org/)

Pour Mac, il  y a un problème de  version de Java. Il faut alors  se reporter au
paquet          disponible           sur          le           [site          de
l'UTC](https://www.utc.fr/~mottelet/scilab_for_macOS.html)



Sur  le site  de Scilab  référencé  ci-dessus on  trouve un  [index général  des
commandes](https://help.scilab.org/docs/6.0.2/fr_FR/index.html) avec l'aide associée.

Il       est      conseillé       sur       vos      machines       d'[installer
Jupyter](https://openclassrooms.com/fr/courses/4452741-decouvrez-les-librairies-python-pour-la-data-science/5559646-installez-jupyter-sur-votre-propre-ordinateur)
et le [noyau Scilab](https://github.com/Calysto/scilab_kernel)


Il faut (peut-être) )savoir que Scilab (comme matlab son concurrent non libre) est
basé sur un noyau de 
fonctions   en   Fortran   ou   en   C  déjà   compilées   donc   rapides
d'utilisation.  En revanche,  tout ce  que vous  créerez  vous-même sera
interprété  ce   qui  ralentira  l'exécution  par   rapport  à  d'autres
langages. De  plus, la gestion  des nombres n'est pas  toujours optimale
car ils  sont codés  en complexes  double précision  ce qui  peut être
lourd.

## Scilab comme super calculatrice


Sans Jupyter, scilab ressemble à ça:

![écran scilab](./IMG/ecranscilab.png)

C'est la console centrale qui nous intéressera au tout début. 


Avec Jupyter:



![écran jupyter](./IMG/ecranjupyter.png)


Scilab peut fonctionner de manière classique:

```scilab
--> 3 + 2
 ans  =

   5.
```

Si on ajoute un «;» à la  fin de la ligne, celle-ci est lue par SCILAB
mais le résultat n'est pas affiché: c'est le mode silencieux.

Mais Scilab fait du calcul numérique; les résultats sont donc donnés
sous forme de *nombres à virgule flottante*:

```scilab
--> 1 + sqrt(2)
 ans  =

   2.4142136
```

On ne  peut pas recommencer  un calcul en  plaçant le curseur  de la
souris sur une ligne précédente déjà évaluée (sauf sous Jupyter). En revanche, avec les
flèches haut et bas, on peut naviguer dans l'historique et
réafficher des commandes précédentes.


Par défaut, les  résultats sont affichés avec 10  caractères. Pour avoir
plus de précision, on utilise `format`:

```scilab
--> format(20); 1 + sqrt(2)
 ans  =

   2.41421356237309492
```


On peut vouloir utiliser l'écriture scientifique d'un nombre: il faut
préciser l'option `'e'`:

```scilab
--> format('e',20); 1+sqrt(2)
 ans  =

   2.4142135623731D+00
```


Le `D+00` signifie mathématiquement $`\times 10^0`$.

On   peut  revenir  à   la  configuration   par  défaut   avec  l'option
`'v'`:


```scilab
 --> format('v',10)
 ```
 
Certaines constantes utiles sont déjà implémentées comme $`\pi`$, $`\exp(1)`$:

```scilab
--> %pi
 %pi  = 

   3.1415927

--> %e
 %e  = 

   2.7182818
```



Pour  les fonctions usuelles,  la syntaxe  est assez  standard. Veuillez
toutefois noter que le logarithme népérien se note `log`.

Expliquez les réponses données par Scilab:

```scilab
--> log(%e^2)
 ans  =

   2.

--> sin(%pi)
 ans  =

   1.225D-16
   

--> (3 + %i)^2
 ans  =

   8. + 6.i

--> abs(1 + %i)
 ans  =

   1.4142136
```


## Help!

Vous  pouvez obtenir  de l'aide  avec la  commande `help`  suivie du  nom de  la
commande que vous souhaitez mieux connaître:

```scilab
--> help log
```

ou tapez sur `F1` pour avoir tout le catalogue.


## Un brin d'informatique

### Types


Scilab travaille  avec des objets de  différents types et  ne les traite
pas de la même manière. Normalement il faudrait distinguer:

* les réels et les complexes;
* les booléens: `%T` pour vrai (True) et `%F` pour faux (False);
* les polynômes;
* les chaînes de caractères entre apostrophes;
* les listes;
* les fonctions;
* et bien d'autres encore...


Mais ce qu'on obtient avec `typeof` est plus sommaire:

```scilab

--> typeof(%e)
 ans  =

  "constant"

--> typeof('%e')
 ans  =

  "string"

--> typeof(2.7182818)
 ans  =

  "constant"
```

### Opérateurs


Scilab possède les opérateurs arithmétiques habituels `(+), (-), (*), (/)`
mais aussi une division inversée `(\)` et les
opérateurs pointés dont nous parlerons plus tard.

Il  sera également important  d'utiliser les  opérateurs *booléens*
qui testent  l'égalité `(==)`, la  non-égalité `(<>)`, les
inégalités strictes et larges `(<), (>), (<=), (>=)`. Le résultat
est un booléen (`%T` ou `%F`):

```scilab

--> 1 + 1 + 1 == 3
 ans  =

  T

--> 0.1 + 0.1 + 0.1 == 0.3
 ans  =

  F
  
--> 0.1 + 0.1 + 0.1 > 0.3
 ans  =

  T

--> 0.1 + 0.1 == 0.2
 ans  =

  T
```




Enfin il existe des opérateurs logiques qui seront également utiles dans
les tests: la négation `~`, l'opérateur ET (`&`) et
l'opérateur OU (`|`). Par exemple:

```scilab

--> ~(3 < 3) == (3 >= 3)
 ans  =

  T
```


### Affectation


Pour donner un nom à une  case mémoire où nous voulons stocker un objet,
on utilise `=`:

```scilab
--> a = 3;

--> a^2
 ans  =

   9.

--> ans^2
 ans  =

   81.

--> a
 a  = 

   3.

--> clear a

--> a

Variable non définie : a
```

## Définir une fonction


On peut définir toutes sortes  de fonctions, d'une ou plusieurs variables,
numériques ou non à l'aide de la syntaxe suivante:


```scilab
--> function y = truc(x)
     y = x*log(x)
   endfunction
   
--> truc(2)
 ans  =

   1.3862944

--> truc(1)
 ans  =

   0.
```


On  peut  utiliser une  écriture  plus  condensée pour  des  fonctions
numériques par exemple:


```scilab

--> deff('y = f(t)' , 'y = sqrt(1 + t^2)')

--> f(1)
 ans  =

   1.4142136
```


## Vecteurs

L'objet de  base sous SCilab est  le vecteur (en  fait la matrice comme  nous le
verrons  plus tard  et ce  qui  explique que  le concurent  non-libre de  Scilab
s'appelle MATlab).

```scilab

--> va = [1 3 74 100*%pi]
 va  = 

   1.   3.   74.   314.15927
```

Si on veut que ces valeurs soient incrémentées régulièrement:


```scilab
--> x = [0:2:11]
 x  = 

   0.   2.   4.   6.   8.   10.
```


On obtient ainsi  une suite arithmétique de nombres  de premier terme 0,
de raison 2 et inférieurs à 11.


Par défaut, l'incrément est de 1:

```scilab
--> y = [0:9]
 y  = 

   0.  1.  2.  3.  4.  5.  6.  7.  8.  9.
```


L'incrément peut être négatif et non entier:

```scilab
--> X = [0:-2.5:-13]
 X  = 

   0.  -2.5  -5.  -7.5  -10.  -12.5
```


On a  facilement les  valeurs de quelques  suites obtenues à  partir des
précédentes:

```scilab
--> x
 x  = 

   0.   2.   4.   6.   8.   10.

--> x^2
 ans  =

   0.  4.  16.  36.  64.  100.
```

On peut extraire une valeur de ces vecteurs:

```scilab

--> x(3)
 ans  =

   4.
```

ou une partie:

```scilab

--> x(1:3)
 ans  =

   0.   2.   4.
```



Le dernier élément d'une matrice est obtenu à l'aide de `$`:

```scilab
--> x($)
 ans  =

   10.
```

On peut changer une composante:


```scilab

--> x
 x  = 

   0.   2.   4.   6.   8.   10.

--> x(2) = 20
 x  = 

   0.   20.   4.   6.   8.   10.
```




On peut extraire des composantes vérifiant une condition avec la
commande `find` qui renvoie les rangs répondant à la demande:


```scilab

--> find(x > 5)
 ans  =

   4.  5.  6.
```


On peut ajouter des composantes à un vecteur:

```scilab

--> x
 x  = 

   0.   2.   4.   6.   8.   10.

--> xnew = [x,42]
 xnew  = 

   0.   2.   4.   6.   8.   10.   42.

--> xbig = [x, [100, 101]]
 xbig  = 

   0.   2.   4.   6.   8.   10.   100.   101.
```




On peut effectuer des opérations entre ces vecteurs. Par exemple,
essayons de les multiplier terme à terme:


```scilab
--> x
 x  = 

   0.   2.   4.   6.   8.   10.

--> X
 X  = 

   0.  -2.5  -5.  -7.5  -10.  -12.5

--> x*X

Dimensions ligne/colonne incohérentes.

--> x .* X
 ans  =

   0.  -5.  -20.  -45.  -80.  -125.
```


Attention!  Les opérateurs arithmétiques `(*), (/)` et `\`
effectuent  des opérations  sur des  *matrices*. Nous  verrons bientôt  (ou vous
l'avez vu en spécialité math en terminale...) que ces opérations 
n'ont presque rien à voir avec celles sur les entiers.  En
particulier, `a*b <> b*a` en général.

Pour effectuer ces dernières opérations «~terme à terme~», il faut utiliser les
opérateurs pointés sans  oublier de laisser une espace  entre le premier
terme et le point.



On dispose d'un vecteur `v = [1 2 3 4 5]` et on voudrait obtenir
`[f(1) f(2) f(3) f(4) f(5)]`  avec `f` une fonction numérique: il
suffit d'écrire `f(v)`...



```scilab
--> deff('y = f(x)', 'y = 2*x')

--> v = [1:5]
 v  = 

   1.   2.   3.   4.   5.

--> f(v)
 ans  =

   2.  4.  6.  8.  10.
```


On peut facilement obtenir la somme ou le produit des composantes:

```scilab
--> sum(v)
 ans  =

   15.

--> prod(v)
 ans  =

   120.
```

Expliquez ce résultat par exemple:

```scilab

--> sum(2 .* [1:10])
 ans  =

   110.
```


Le nombre de composantes:

```scilab

--> length(v)
 ans  =

   5.
```




## Les graphiques


On commence par créer un vecteur de 10 000 valeurs régulièrement espacées
entre 0 et 100:


```scilab
x = linspace(0,100,10000);
```


On aurait pu aussi bien poser:

```scilab
 --> x = 0:.01:100
```


Nous  voulons  obtenir  la   représentation  graphique  de  la  fonction
$`x\mapsto x\cdot \sin(x)`$ sur  [0 , 100]:


```scilab
plot(x,x .*sin(x))
```


Pour  rajouter d'autres  courbes  sur la  figure,  nous avons  plusieurs
possibilités:



```scilab
--> plot(x,x .*sin(x),x,x,x,-x)
```

ou 

```scilab

--> t = [0:0.1:100]';

--> plot(t,[t .* sin(t), t, -t])
```

Notez  bien l'apostrophe  finale (qui  correspond  à la  transposition que  nous
verrons en calcul matriciel).



On peut modifier  le style des courbes à partir des  menus de la fenêtre
graphique ou en ligne de commandes:


```scilab
 --> X = -%pi:%pi/10:%pi;
 --> plot(X,sin(X),"+-b")
```



L'option  `+-b`  signifie qu'on  représente  chaque  point par  une
croix (`+`), qu'on les relie par un trait continu (`-`), en bleu (`b`).



Il existe d'autres raccourcis:

|Couleur|jaune|magenta|cyan|rouge|vert|bleu|blanc|noir|
|-------|-----|-------|----|-----|----|----|-----|----|
|Symbole| y|m|c|r|g|b|w|k|


|Ligne| plein| tirets | pointillés | mixte |
|-----|------|--------|------------|-------|
|Symbole| `-` | `--` | `:` | `-.` |



|Point| plus| rond | astérisque | point | croix|
|-----|------|--------|------------|-------|---|
|Symbole| `+` | `o` | `*` | `.` | `x` |





N'hésitez pas  à explorer l'aide  sur `gce` et  `gda` pour
plus de  "configurabilité".



On peut avoir besoin de tracer une ligne polygonale: elle sera définie
à partir de la liste de ses abscisses et celle de ses ordonnées.


```scilab
--> x = [1 2 5 10];

--> y = [-1 3 0 5];

--> plot(x, y)
```


## Programmation


### Conditionnelles


Construisons  par exemple une  fonction donnant  la valeur  absolue d'un
nombre:

```scilab
--> function y = ABS(x)
       if x >= 0 then
        y=x
      else
        y=-x
      end
   endfunction
   
--> ABS(-3)
 ans  =

   3.

--> ABS(3)
 ans  =

   3.
```

### Éditeur de programme



Il  existe  un éditeur  de  texte  dédié à  Scilab  (`Scinotes`)  qui permet  de
travailler à part sur son programme avant de l'éxécuter.

On tape ensuite sur `F5` pour l'enregistrer et l'exécuter.



### Boucles inconditionnelles



Par exemple, calculons la somme S des 25 premiers entiers naturels (il faut
commencer par initialiser `S`):

```scilab

--> S = 0;

--> for i = 1:25 do 
  > S = S + i;
  > end

--> S
 S  = 

   325.
```

 L'emploi du mode silencieux après le `S = S + i` permet
d'éviter d'afficher tous les résultats intermédiaires. 



### Boucles conditionnelles




Reprenons le calcul de la somme des 25 premiers entiers:


```scilab
--> S = 0;

--> i = 1;

--> while i <= 25 do
  > S = S + i;
  > i = i + 1;
  > end

--> S
 S  = 

   325.
```

### Des boucles cachées


Il est  plus rapide, quand  c'est possible, d'itérer sur  des matrices
sans utiliser de boucles.

Il est  plus rapide, quand  c'est possible, d'itérer sur  des matrices
sans utiliser de boucles. 

Pour faire le calcul précédent:

```scilab
S = sum(1:25)
```

<!--

Considérons  par  exemple la  méthode  des  rectangles pour  approcher
l'intégrale d'une fonction sur un intervalle donné. On doit calculer:

$` 
\displaystyle\sum_{k=1}^{n} \frac{b-a}{n}\times f \left(a+k\times  \frac{b-a}{n} \right) 
`$

Une première idée serait d'écrire:

```scilab
function R = Rectangles1(f,a,b,n)
  h = (b-a)/n
  R = 0
  for k = 1:n do
    x = a + k*h
    R = R + h*f(x)
  end
endfunction
```


Mais il sera beaucoup plus efficace en temps d'exécution d'écrire:

```scilab
function R = Rectangles(f,a,b,n)
  h = (b-a)/n
  X = a + h*[1:n]
  R = h*sum(f(X))
endfunction
```
-->


## Savoir faire en vrac


*  créer des variables avec `=`, en changer la valeur, les utiliser dans les calculs successifs.
* échanger le contenu deux variables en stockant dans une variable \quote{intermédiaire}.
*  différencier les opérations standards des opérations pointées pour les vecteurs
* différencier `,` et `;` dans la création de matrices/vecteurs.
*  commenter son code avec `//`.
* manipuler les booléens `%T` (true) et `%F` (false) avec les opérateurs logiques `&` (et), `|` (ou), `∼` (non). Les stocker dans des variables ou les utiliser pour des structures conditionnelles ou itératives `while`.
*  effectuer des tests logiques avec les opérateurs `==`, `<>`, ` >`, ` <`, ` >=`, ` <=`
* différencier ` =` et ` ==`.

* créer un vecteur vide avec la commande [].
* créer un vecteur ligne ou colonne. Y stocker des résultats successifs ` v = [v, k]`. Y lire la k-ième valeur  ` v(k)`, la modifier. ` v(k) = v(k) +1`

* obtenir la taille (i.e. le nombre d’éléments qu’il contient) d’un vecteur avec la fonction ` length`.

* créer un vecteur constitué des entiers de $`m`$ à $`n`$ avec la commande ` m:n` (et ` m:p:n` si on désire les espacer par
pas de p).
* créer un vecteur constitué des $n$ réels subdivisant [a,b] en $`n-1`$ intervalles de même amplitude avec ` linspace(a,b,n)`.
*  utiliser les commandes ` rand(1,n)`, ` ones(1,n)`, ` zeros(1,n)`. En particulier, utiliser un vecteur initialisé par `  v = zeros(1,n)` et utiliser les coordonnées comme compteurs.
* effectuer des opérations arithmétiques (coordonnées par coordonnées) sur des vecteurs de même taille
constitués de nombres avec ` +`, ` -`, ` .*`, ` ./`, ` .**` ou  ` .∧` ainsi que des tests de comparaison.
* appliquer une fonction de référence à un vecteur de nombres coordonnées par coordonnées (exemple
` cos([\%pi*[0:0.1:1])`).


* Écrire un programme, le sauvegarder et l'exécuter avec Scinotes.
* utiliser les commandes ` input` et ` disp`.
*  créer une structure conditionnelle avec ` if...then...else...end` et éventuellement ` elseif`

* créer une structure répétitive avec une boucle ` for...end` ou une boucle ` while...`.
* utiliser un tableau pour visualiser l'évolution des variables au cours de l'exécution du programme.
* créer une fonction avec la syntaxe ` function ... endfunction`, la fonction pouvant prendre plusieurs variables en entrée, et renvoyer plusieurs variables. Pas de ` input` car les entrées sont définies dans l'entête et renseignées au moment de l'appel de la nouvelle fonction. Pas de ` disp` car la fonction renvoie un résultat qu'on choisit *ensuite` d'afficher ou non*

* afficher **toutes** les sorties lorsqu’on exécute une fonction renvoyant plusieurs arguments de sortie.

* calculer n ! avec une boucle for ou avec ` prod(1:n)`
```scilab
function f = fact(n)
	f = 1
	for k = 1:n
		f = f*k
	end
endfunction
```

* calculer $`\binom np`$ avec une boucle ` for` :
```scilab
function y = binom(n,p)
	y = 1;
	for k = 0:p-1
		y = y*(n-k)/(p-k);
	end
endfunction
```



* calculer un terme de rang donné d’une suite $`(u_ n )_{ n∈\N}`$ définie par récurrence.

* Stocker dans un vecteur tous les termes de rang inférieur à en entier n donné d'une suite $`(u_ n )_{ n∈\N}`$. On complète les coordonnées au fur et à mesure avec, par exemple ` U = [U, u]`. Faire en sorte si possible que ` U[k]` soit égal à $`u_k`$. Il y a parfois un décalage de 1 si u commence avec $`u_0`$.


* sommer les termes d’une suite. On peut ajouter à la volée avec une commande type ` S = S + u` ou, si les termes sont stockés dans un vecteur ` U`, avec ` sum(U)`

* multiplier les termes d’une suite. On remplace juste ` s=s+u` par ` s=s*u` dans les scripts du point précédent.

*  utiliser un compteur dans une boucle while pourobtenir le nombre d’itérations nécessaires à l'obtention d'une condition donnée. 

Application importante : Étant donnée une suite convergeant vers un réel $`\ell`$, déterminer le nombre d'itérations nécessaires pour que $`\abs{u_n-\ell\varepsilon`$ pour un $`\varepsilon`$ donné.


* effacer les fenêtres graphiques en cours avec ` clf();`

* Tracer la ligne brisée reliant les points donc les coordonnées sont stockées dans deux vecteurs ` x` et ` y` de même dimension avec ` plot(x,y,’opt’)` ou ` plot2d(x,y,options)`. Les options ne sont pas à connaître.

*  tracer la courbe d'une fonction f sur l’intervalle [a,b] avec la commande ` x = linspace(a,b,1000)` puis les commandes ` plot(x,f(x),’opt’)` ou ` plot2d(x,f(x),options)`. 

On peut aussi calculer les images des coordonnées de ` x` avec

```scilab
y = [ ]
for a = x
	y = [y, f(a)]
end
```
Ensuite, ` plot(x,y)` fait le travail

* tracer un diagramme à barres avec ` bar(x,y)`, où ` x` est un vecteur contenant les abscisses des catégories et
` y` un vecteur contenant les effectifs respectifs de ces catégories. Ici les données ont été traitées/comptées
*  tracer un histogramme avec ` histplot(n,x)`, où ` x` est un vecteur contenant des données réelles (brutes) et $n$ un
entier naturel strictement positif correspondant au nombre de classes (de même amplitude par défaut).

* représenter graphiquement (pratique pour conjecturer une éventuelle convergence) les termes d’une suite
$`(u_ n )_{ n∈\N}`$. Le script suivant représente graphiquement les n
premiers termes de la suite de terme initial x :
```scilab
u = x; // valeur de u_0
U = [u];
for k = 1:n
	u = calcul du terme suivant en fonction de u et k;
	U = [U,u]
end
plot(0:n,U)//On trace les premiers termes
```



# Exercices




## Scilab à EML

On pose $`u_0=4`$ et $`\forall n \in \N,\ u_{n+1}=\ln(u_n)+2`$.

Écrire une  fonction Scilab d'en-tête  `function u  = suite(n)` qui,  prenant en
argument un entier naturel `n`, renvoie la valeur de $`u_n`$.



## Scilab à EML

On introduit la suite $`(u_n)_{n\in\N}`$ définie par: 

$`u_1=1\ \ (\forall n\in\N^*)(u_{n+1}=u_n+\frac{1}{n^2u_n}=\frac{1}{n}f(nu_n))`$

Recopier et compléter les lignes 2 et 3 de la fonction Scilab suivante afin que,
prenant  en argument  un  entier n  de  $`\N^*`$, elle  renvoie  la valeur  de
$`u_n`$.

```scilab
function u = suite(n)
	u = 1
	for k = ...
		u = ...
	end
endfunction
```

Tester avec n valant 2, 3 et 4.




## ECRICOME 2020


On considère la suite $`(u_n)_{n\in\N}`$ définie par 

$`u_0=0,\ u_1=1\ \ (\forall n\in\N^*)(u_{n+1}=7u_n+8u_{n-1})`$

Recopier et compléter  les trois lignes incomplètes du  script Scilab ci-dessous
pour qu'il calcule $`u_n`$ pour n un entier naturel entré par l'utilisateur:


```scilab
n = input('Entrer un entier n : ')
u = 0; v = 1
for k = ...
	w = u
	u = ...
	v = ...
end
disp(u)
```



## ECRICOME 2020

Recopier et compléter  le script du programme Scilab suivant  pour qu'il affiche
le plus petit entier naturel non nul n tel que $`u_n \leqslant 1/1000`$ avec  $`(u_n)_{n\in\N^*}`$ définie par 

$`u_1=1\ \ (\forall n\in\N^*)(u_{n+1} = f(u_n) = \frac{u_n}{1+u_n+u_n^2})`$

```scilab
function y = f(x)
	y = x / (1 + x + x^2)
endfunction

u = ...
n = ...
while u ....
		u = ...
		n = ...
end

disp(...)
```

## Scilab à HEC

Expliquer le code suivant:

```scilab
nmax = 30
U = zeros(nmax, 1);
U(1) = 0;
for i=1:nmax
    U(i + 1) = cos(U(i))
end
plot(0:nmax, U, '*')
```


## Scilab à HEC

Soit *f* la fonction définie sur $`[-1,1]`$ par $`f(x)=\lfloor 2|x|\rfloor`$.

Indiquer  comment obtenir  une représentation  graphique  de la  fonction *f*  à
l'aide de Scilab. Quelle sera l'allure de la figure obtenue?

## Scilab à HEC

On note *f* la fonction définie sur **R** par:

$`\forall x\in R, \quad f(x)={\rm Min}\big\{|x-n|; n\in Z\big\}`$

1. Calculer $`f(x)`$ lorsque *x* est compris entre -1/2 et 1/2.
2.  Donner   une  formule   permettant  de   calculer  $`f(x)`$   sans  utiliser
   Min. 
3. Donner  une représentation graphique de  la fonction *f* et  écrire un script
   Scilab permettant de la tracer.



## Scilab à HEC

Dans une classe de 30 élèves,  on considère une expérience consistant à demander
à chaque  élève sa date d'anniversaire.  La suite de l'expérience  (simulée 1000
fois sur ordinateur) est décrite par le programme Scilab suivant:


```scilab
Nexp = 1000 ; Neleve = 30 ; test = 0 ;
for n = 1:Nexp
  anniv = zeros(Neleve, 1)
  for i = 1:Neleve
    anniv(i) = floor(365 * rand()) + 1 ;
  end
  anniv = gsort(anniv) ; // gsort : tri par ordre croissant
  ok = 0 ;
  for j = 1:Neleve - 1
      if anniv(j) == anniv(j + 1) then
          ok = 1 ;
      end
  end
  test = test  + ok ;
end
disp(test / Nexp) ;
```

Écrire un  programme Scilab permettant de déterminer le  nombre d'élèves à
  partir duquel la valeur renvoyée par le code ci-dessus dépasse 0,5.
  
